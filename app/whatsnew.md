#Version 0.13.1
*BugFixes*
- Fixe bug in Inflow when uploading perfs (Description field empty) that was causing error when transferring perf to perf-trace template

#Version 0.13.0
*Features*
- Add edit description button
- Remove brackets around ticket number in title when creating task from youtrack

#Version 0.12.1

*Features*
- Button added to re-upload data into Inflow

#Version 0.12.0

*Features*

- Allow duplicate on current day
- Upgrade to electron 0.36.7

*BugFixes*

- Restore youtrack project list

#Version 0.11.0

*Features*

- View performances details in dahsboard, when clicking on a project duration (graph), the performances linked are shown
- Improve PJM Tools screen, the results now include the Perf-trace and reserve

#Version 0.10.7

*BugFixes*

- Fix bug in inflow selection (PJM tools) , the query returned too many records and gave wrong results.

*Features*

- Upgrade to atom electron 0.29.2

#Version 0.10.6

*BugFixes*

- Fix tooltip bug in dashboard (on the first line)
- Fix trycatch error on loading

*Features*

- Add what's new link on main page with all info about new features in the version
- Sort item in dashboard according to total time
- Upgrade to atom electron 0.28.1
