var app = require('app');
var browserWindow = require('browser-window');
var ipc = require('ipc');
var applicationMenu = require('./applicationMenu');
var menu = require('menu');

require('crash-reporter').start();

var mainWindow = null;

app.on('window-all-closed', function () {
  app.quit();
});

app.on('ready', function () {
  mainWindow = new browserWindow(
    {
      'width': 1024,
      'height': 768,
      'min-width': 1024,
      'min-height': 768
    }
  );
  mainWindow.loadUrl('file://' + __dirname + '/index.html');
  //mainWindow.openDevTools();
  mainWindow.on('closed', function () {
    mainWindow = null;
  });

  var menuBuilt = {};

  if (process.platform == 'darwin') {
    console.log('set osx menu');
    menuBuilt = menu.buildFromTemplate(applicationMenu.osx);
  }else{
    menuBuilt = menu.buildFromTemplate(applicationMenu.win);
  }
  menu.setApplicationMenu(menuBuilt);

});

ipc.on('quit', function (event, arg) {
  console.log('Application Quit asked');
  app.quit();
});
