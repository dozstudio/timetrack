var app = require('app');  // Module to control application life.
var BrowserWindow = require('browser-window');  // Module to create native browser window.

var macosTemplate = [
    {
      label: 'Timetrack',
      submenu: [
        //{
        //  label: 'About Electron',
        //  selector: 'orderFrontStandardAboutPanel:'
        //},
        //{
        //  type: 'separator'
        //},
        //{
        //  label: 'Services',
        //  submenu: []
        //},
        //{
        //  type: 'separator'
        //},
        {
          label: 'Hide Electron',
          accelerator: 'Command+H',
          selector: 'hide:'
        },
        {
          label: 'Hide Others',
          accelerator: 'Command+Shift+H',
          selector: 'hideOtherApplications:'
        },
        {
          label: 'Show All',
          selector: 'unhideAllApplications:'
        },
        {
          type: 'separator'
        },
        {
          label: 'Quit',
          accelerator: 'Command+Q',
          click: function() { app.quit(); }
        },
      ]
    },
    {
      label: 'Edit',
      submenu: [
        {
          label: 'Undo',
          accelerator: 'Command+Z',
          selector: 'undo:'
        },
        {
          label: 'Redo',
          accelerator: 'Shift+Command+Z',
          selector: 'redo:'
        },
        {
          type: 'separator'
        },
        {
          label: 'Cut',
          accelerator: 'Command+X',
          selector: 'cut:'
        },
        {
          label: 'Copy',
          accelerator: 'Command+C',
          selector: 'copy:'
        },
        {
          label: 'Paste',
          accelerator: 'Command+V',
          selector: 'paste:'
        },
        {
          label: 'Select All',
          accelerator: 'Command+A',
          selector: 'selectAll:'
        },
      ]
    },
    {
      label: 'View',
      submenu: [
        {
          label: 'Reload',
          accelerator: 'Command+R',
          click: function() { BrowserWindow.getFocusedWindow().restart(); }
        },
        {
          label: 'Toggle Full Screen',
          accelerator: 'Ctrl+Command+F',
          click: function() { BrowserWindow.getFocusedWindow().setFullScreen(!BrowserWindow.getFocusedWindow().isFullScreen()); }
        },
        {
          label: 'Toggle Developer Tools',
          accelerator: 'Alt+Command+I',
          click: function() { BrowserWindow.getFocusedWindow().toggleDevTools(); }
        },
      ]
    },
    {
      label: 'Window',
      submenu: [
        {
          label: 'Minimize',
          accelerator: 'Command+M',
          selector: 'performMiniaturize:'
        },
        {
          label: 'Close',
          accelerator: 'Command+W',
          selector: 'performClose:'
        },
        {
          type: 'separator'
        },
        {
          label: 'Bring All to Front',
          selector: 'arrangeInFront:'
        },
      ]
    },
    {
      label: 'Help',
      submenu: [
        {
          label: 'Learn More',
          click: function() { require('shell').openExternal('http://electron.atom.io') }
        },
        {
          label: 'Documentation',
          click: function() { require('shell').openExternal('https://github.com/atom/electron/tree/master/docs#readme') }
        },
        {
          label: 'Community Discussions',
          click: function() { require('shell').openExternal('https://discuss.atom.io/c/electron') }
        },
        {
          label: 'Search Issues',
          click: function() { require('shell').openExternal('https://github.com/atom/electron/issues') }
        }
      ]
    }
  ];

var winTemplate = [
    {
      label: '&File',
      submenu: [
        {
          label: '&Open',
          accelerator: 'Ctrl+O',
        },
        {
          label: '&Close',
          accelerator: 'Ctrl+W',
          click: function() { BrowserWindow.getFocusedWindow().close(); }
        },
      ]
    },
    {
      label: '&View',
      submenu: [
        {
          label: '&Reload',
          accelerator: 'Ctrl+R',
          click: function() { BrowserWindow.getFocusedWindow().restart(); }
        },
        {
          label: 'Toggle &Full Screen',
          accelerator: 'F11',
          click: function() { BrowserWindow.getFocusedWindow().setFullScreen(!BrowserWindow.getFocusedWindow().isFullScreen()); }
        },
        {
          label: 'Toggle &Developer Tools',
          accelerator: 'Alt+Ctrl+I',
          click: function() { BrowserWindow.getFocusedWindow().toggleDevTools(); }
        },
      ]
    },
    {
      label: 'Help',
      submenu: [
        {
          label: 'Learn More',
          click: function() { require('shell').openExternal('http://electron.atom.io') }
        },
        {
          label: 'Documentation',
          click: function() { require('shell').openExternal('https://github.com/atom/electron/tree/master/docs#readme') }
        },
        {
          label: 'Community Discussions',
          click: function() { require('shell').openExternal('https://discuss.atom.io/c/electron') }
        },
        {
          label: 'Search Issues',
          click: function() { require('shell').openExternal('https://github.com/atom/electron/issues') }
        }
      ]
    }
  ];

module.exports = {
  osx: macosTemplate,
  win: winTemplate
}