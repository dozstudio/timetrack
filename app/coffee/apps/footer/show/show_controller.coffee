App.module "FooterApp.Show", (Show, App, Backbone, marionette, $, _)->
  class Show.Controller extends Marionette.Controller

    showFooter: ()->
      @footerView = new Show.FooterView()

      @footerView.on 'check:update', =>
        @checkUpdates()

      @footerView.on 'whatsnew', =>
        @showWhatsNew()

      App.footerRegion.show @footerView
      @checkUpdates()

    checkUpdates: ->
      App.log("Checking for updates...")
      client = require('ftp')
      ftp = new client()
      ftp.on 'error', (err)=>
        App.log err
      ftp.on 'ready', =>
        try
          ftp.list (err, list)=>
            currentVersion = require('./package.json').version
            versionFound = currentVersion
            semver = require('semver')
            _.forEach list, (dir)=>
              if(semver.gt(dir.name, versionFound))
                versionFound = dir.name
            if currentVersion != versionFound
              App.log("New version #{versionFound} has been found")
              @footerView.setNewVersionMessage("http://timetrack.no-ip.org/#{versionFound}",
                versionFound)
              App.notify "New version '#{versionFound}' available at http://timetrack.no-ip.org/#{versionFound}"
              clearInterval(@updaterInterval)
            ftp.end()
        catch
          App.log "unable to list updates"
      try
        ftp.connect
          host: 'dozstudio.ddns.net'
          user: 'timetrack'
          password: 'timetrack2015'
      catch
        App.log "unable to get update list"

    showWhatsNew: ->
      fs = require('fs')
      fileName = "#{__dirname}/whatsnew.md"
      App.log "Try to read file #{fileName}"
      fs.readFile(fileName,(err,data)=>
        if err
          App.log "error : #{err}"
        view = new Show.WhatsNewView(
          data: data.toString()
        )
        App.setBottomPanel(view)
      )