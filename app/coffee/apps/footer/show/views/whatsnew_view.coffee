App.module "FooterApp.Show", (Show, App, Backbone, Marionette, $, _)->
  class Show.WhatsNewView extends Marionette.ItemView
    template: 'footer/show/whatsnew'

    events:
      'click #closePopup': 'closePopup'

    closePopup: ->
      App.hideBottomPanel()

    initialize:(options)=>
      mk = require('markdown')
      @data = mk.markdown.toHTML(options.data)

    serializeData:=>
      data: @data
