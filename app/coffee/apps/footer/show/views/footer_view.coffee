App.module "FooterApp.Show", (Show, App, Backbone, Marionette, $, _)->
  class Show.FooterView extends Marionette.ItemView
    template: 'footer/show/footer'

    triggers:
      'click #btWhatsNew': 'whatsnew'

    events:
      'click #versionUrl': 'versionUrlClick'
      'click #lnkUpdateCheck': 'updateCheckClick'

      'dblclick .container': 'debugModeClick'

    initialize: ->
      #read package.json
      @version = require('./package.json').version

    setNewVersionMessage: (url, newVersion)->
      @versionUrl = url
      @newVersion = newVersion
      @render()

    updateCheckClick: (e)->
      e.preventDefault()
      @trigger('check:update')

    versionUrlClick: (e)=>
      e.preventDefault()
      url = @versionUrl
      shell = require('shell');
      shell.openExternal(url);


    debugModeClick: (e)=>
      e.preventDefault()
      #TODO:show dev tools
#      win = @gui.Window.get()
#      win.showDevTools()

    serializeData: ->
      version: @version
      newVersion: @newVersion
      newVersionUrl: @versionUrl
      electronVersion: process.versions['electron']
      chromeVersion: process.versions['chrome']
      adminMode: App.request("preference:get","AdminMode")

