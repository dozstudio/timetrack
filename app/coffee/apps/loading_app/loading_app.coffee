App.module "LoadingApp", (LoadingApp, App, Backbone, Marionette, $, _)->

  LoadingApp.on "start", ->
    App.log 'Loading app Start'

    lView = new LoadingApp.Views.LoadingView()
    App.setMainView(lView)

    lView.setMessage "Checking preferences..."
    if App.request "preferences:load"
      App.vent.trigger "preferences:ok"
      #starting Entities Module

      lView.setMessage "Loading inflow data..."
      $.when(
        App.request("inflow:types"),
        App.request("inflow:tree")
      ).done ()=>
        App.vent.trigger "tracker:show"
    else
      alert('you preferences are not set, please fix!')
      App.vent.on "prefs:saved", ->
        App.vent.off "prefs::saved"
#        App.start()

      App.vent.trigger "prefs:list"
