App.module "PreferencesApp", (PreferencesApp, App, Backbone, Marionette, $, _)->
  class PreferencesApp.Model extends Backbone.Model

    checkConsistency: ()->
      #mandatory fields
      result = @setDefaultField('DataPath', App.Utils.getUserHome() + "/Documents/TimeTracker/")
      result = result & @setDefaultField('DatabasePath',
        App.Utils.getUserHome() + "/Documents/TimeTracker/db.sqlite")
      result = result & @setDefaultField('YoutrackUrl')
      result = result & @setDefaultField('YoutrackUser')
      result = result & @setDefaultField('YoutrackPassword')
      result = result & @setDefaultField('DefaultYoutrackSearch', '#unresolved')
      result = result & @setDefaultField('RoundTimeMinutes', 5)
      result = result & @setDefaultField('InflowUrl', 'https://appli.pulsar.be/inFlowTracerPlus-rest')
      result = result & @setDefaultField('InflowUser')
      result = result & @setDefaultField('InflowPassword')
      result = result & @setDefaultField('InflowDatabaseUrl',
        "postgres://postgres:postgres@tucana.pulsar.be/INFLOW_TRACER_PROD")
      result = result & @setDefaultField('InactivityTimeOutInMinutes', "10")
