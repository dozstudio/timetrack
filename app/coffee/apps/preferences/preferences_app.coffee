App.module "PreferencesApp", (PreferencesApp, App, Backbone, Marionette, $, _)->


  App.on "before:start", =>
    App.log 'Pref Before Start'
    @ctl = new PreferencesApp.Edit.Controller()

  App.vent.on "prefs:list", ()=>
    @ctl.showPreferencesList()

  #requests handlers

  App.reqres.setHandler 'preference:get', (name)=>
    @ctl.getPreference(name)

  App.reqres.setHandler 'preference:set', (options)=>
    @ctl.setPreference(options.name, options.value)

  App.reqres.setHandler 'preferences:load', ()=>
    @ctl.loadPreferences()

  App.reqres.setHandler 'preferences:save', ()=>
    @ctl.savePreferences()

