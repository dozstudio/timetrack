App.module "PreferencesApp.Edit", (Edit, App, Backbone, Marionette, $, _)->
  class Edit.Controller extends Marionette.Controller

    initialize: ()->
      App.log "Loading default preferences"
      @fs = require('fs')
      mp = require('mkdirp')
      @prefHomePath = App.Utils.getUserHome() + "/Documents/TimeTracker/"
      err = mp.sync(@prefHomePath)
      @fileName = @prefHomePath + "preferences.json"
      @defaultDataPath = @prefHomePath + "DataFiles"
      err = mp.sync(@defaultDataPath)

    loadPreferences: ()->
      exists = @fs.existsSync @fileName
      if(exists)
        #load file
        fileContent = @fs.readFileSync(@fileName, 'utf8')
        @preferences = new App.PreferencesApp.Model(JSON.parse(fileContent))
      else
        @preferences = new App.PreferencesApp.Model()

        @preferences.set('DataPath', @defaultDataPath)

      unless @preferences.checkConsistency()
        return false
      return true

    savePreferences: (trigger)->
      fileContent = JSON.stringify(@preferences.toJSON(), null, 2)
      @fs.writeFileSync(@fileName, fileContent)
      if(trigger)
        App.notify 'Preferences have been saved!'
      App.vent.trigger 'prefs:saved'

    getPreference: (name)->
      try
        val = @preferences.get(name)
      catch
        @loadPreferences()
        val = @preferences.get(name)
      if val
        val
      else
        ""

    setPreference: (name, value)->
      @preferences.set(name,value)
      @savePreferences(false)

    showPreferencesList: ()->
      listView = new Edit.ListView
        model: @preferences

      listView.on 'save', ()=>
        @preferences = listView.model
        @savePreferences()

      App.setMainView(listView)
      App.vent.trigger 'select:link', 'prefs:list'
