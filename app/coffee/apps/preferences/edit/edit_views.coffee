App.module "PreferencesApp.Edit", (Edit, App, Backbone, Marionette, $, _)->
  class Edit.ListView extends Marionette.ItemView
    template: 'preferences/edit/list'

    ui:
      'selector': '#dirselector'

    events:
      'click #btSave': 'save'
      'change #dirselector': 'dirSelectorChanged'

    bindings:
      '#YoutrackUrl': 'YoutrackUrl'
      '#YoutrackUser': 'YoutrackUser'
      '#YoutrackPassword': 'YoutrackPassword'
      '#YoutrackDefaultSearch': 'DefaultYoutrackSearch'
      '#DataPath': 'DataPath'
      '#DatabasePath': 'DatabasePath'
      '#RoundTimeMinutes': 'RoundTimeMinutes'
      '#InflowUrl': 'InflowUrl'
      '#InflowUser': 'InflowUser'
      '#InflowPassword': 'InflowPassword'
      '#InflowDatabaseUrl': 'InflowDatabaseUrl'
      '#InactivityTimeOutInMinutes': 'InactivityTimeOutInMinutes'

    save: ->
      path = @$('#DataPath').val()
      @model.set('DataPath', path)
      @trigger 'save', @model

    dirSelectorChanged: (e, a)->
      @model.set('DataPath', @ui.selector.val())


    onRender: ()->
      @$('#RoundTimeMinutes').mask('99')
      @stickit()

