App.module "TrackerApp", (TrackerApp, App, Backbone, marionette, $, _)->

  TrackerApp.on "start", ->
    @ctl = new TrackerApp.Show.Controller()

  App.vent.on "tracker:show", (date) =>
    unless date
      date = new Date()
    @ctl.showTrackView(date)
