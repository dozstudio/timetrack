App.module "TrackerApp.Show", (Show, App, Backbone, marionette, $, _)->
  class Show.Controller extends Marionette.Controller

    initialize: ->
      App.log "Initialize Controller"
      @model = new Show.Models.DayDetails()
      @collection = @model.get('items')
      @moment = require('moment')

    showTrackView: (date)->

      App.log "Show track view"
      mainView = new Show.MainView()
      App.setMainView(mainView)
      currentDayView = new Show.CurrentDayView
        model: @model
        collection: @collection
      @model.loadFile(date)
      mainView.dayRegion.show currentDayView
      if @moment().format('YYYY-MM-DD') == @moment(date).format('YYYY-MM-DD')
        graphView = new Show.DayGraphView()
        App.log "Setting graph"
        currentDayView.graphRegion.show graphView
        graphView.on 'childview:extract', (view, timeModel)=>
          @model.prepareExtractedTime(timeModel)
        @model.on 'times', (obj)=>
          graphView.setGraphValues(obj)
        @model.sendTimes()

      App.vent.trigger 'select:link', 'tracker:show'

      @listenTo(App.vent,"hb:day:changed",()=>
        @stopListening(App.vent,"hb:day:changed")
        App.hideBottomPanel()
        App.vent.trigger "tracker:show",new Date()
      )

      currentDayView.on 'refreshInflowTime', =>
        @model.computeInflowTotalTime()

      currentDayView.on 'goToPreviousDay', =>
        #stop all
        App.log("Go to previous day")
        currentDayView.model.stopAll()
        mdate = @moment(currentDayView.model.get('daySelected'))
        mdate = mdate.subtract(1, 'days')
        App.vent.trigger "tracker:show", mdate

      currentDayView.on 'goToNextDay', =>
        #stop all
        App.log("Go to next day")
        @model.stopAll()
        mdate = @moment(currentDayView.model.get('daySelected'))
        mdate = mdate.add(1, 'days')
        App.vent.trigger "tracker:show", mdate

      currentDayView.on 'add', =>
        App.log("Add new task")
        newModel = new Show.Models.DayItemModel()
        newModel.set('timeToInject', @model.get('extractedTime'))
        @collection.add newModel

      currentDayView.on 'addAndStart', =>
        App.log("Add new task and start timer")
        @model.stopAll()
        newModel = new Show.Models.DayItemModel()
        newModel.set('timeToInject', @model.get('extractedTime'))
        @collection.add newModel
        newModel.startTimer()

      currentDayView.on 'addPause', =>
        App.log("Add new pause task and start timer")
        @model.stopAll()
        newModel = new Show.Models.DayItemModel()
        newModel.set('pause', true)
        newModel.set('title', 'PAUSE')
        @collection.add newModel
        newModel.startTimer()

      currentDayView.on 'dayChanged', (mom) =>
        App.log("Day changed")
        @model.stopAll()
        App.vent.trigger "tracker:show", mom
        @model.computeInflowTotalTime()

      currentDayView.on 'normalizeTimes', =>
        App.log("Normalize times")
        @model.normalizeTimes()

      currentDayView.on 'childview:start', (childview) =>
        @model.stopAll()
        childview.model.startTimer()

      currentDayView.on 'childview:stop', (childview) =>
        childview.model.stopTimer()

      currentDayView.on 'childview:remove', (childview) =>
        if confirm 'Are you sure you want to delete this task ?'
          @collection.remove childview.model

      currentDayView.on 'childview:inflow:search', (childview, options) =>
        vw = new Show.InflowView(options)
        vw.on 'inflow:selected', (data)=>
          App.log("Selecting inflow project #{data}")
          childview.setInflowSelection(data)
        App.setBottomPanel(vw)

      currentDayView.on 'childview:youtrack:search', (childview) =>
        options = {}
        options.lastProject = @ytLastProjectSelected
        if @ytLastSearchSelected
          options.search = @ytLastSearchSelected
        else
          options.search = App.request 'preference:get', 'DefaultYoutrackSearch'
        vw = new Show.YoutrackView(options)

        vw.on('search', (options)=>
          App.log("Searching youtrack topic #{options.project} / #{options.filter}")
          loadingView = new App.CommonViews.LoadingView()
          vw.resultRegion.show(loadingView)
          $.when(App.request 'youtrack:login').done ()=>
            $.when(App.request('youtrack:searchbyproject', options)).done (results)=>
              @ytLastSearchSelected = options.filter
              @ytLastProjectSelected = options.project
              tableView = new Show.YoutrackTableView
              tableView.on 'childview:youtrack:selection', (cv, data)=>
                childview.setYoutrackSelection(data)
                App.hideBottomPanel()
              tableView.collection = results
              vw.resultRegion.show(tableView)
        )

        App.setBottomPanel(vw)

      currentDayView.on 'childview:inflow:upload', (childview) =>
        mod = childview.model
        $.when(App.request 'inflow:add:prest',
            applicationId: mod.get 'inflowId'
            date: currentDayView.model.get('daySelected')
            minutes: @moment.duration(mod.get('duration')).asMinutes()
            title: mod.get('title')
            comment: 'Added by Timetrack'
            typeId: mod.get('inflowTypeId')
            chargeable: 'Y'
            description: mod.get('description')
        ).done (resultInflow)->
          childview.model.set('uploadedInflow', true)

      currentDayView.on 'childview:youtrack:upload', (childview) =>
        mod = childview.model
        $.when(App.request 'youtrack:login').done =>
          $.when(App.request 'youtrack:upload',
              issueId: mod.get 'youtrackId'
              duration: @moment.duration(mod.get('duration')).asMinutes()
              description: mod.get('title')
          ).done (resultYoutrack)->
            childview.model.set('uploadedYoutrack', true)


      currentDayView.on 'removeTaskSelection', (childview) =>
        currentDayView.model.clearExtractedTime()

      currentDayView.on 'childview:injectTime', (childview) =>

        mod = childview.model
        time = mod.get('timeToInject')
        mod.addDuration(time.get('duration'))
        if(time.get('inactivityId'))
          inactId = time.get('inactivityId')
          inact = @model.get('inactivities').findWhere
            inactivityId: inactId
          if inact
            @model.get('inactivities').remove inact

        @model.clearExtractedTime()

      currentDayView.on 'childview:duplicateToday', (childview) =>
        mod = new Show.Models.DayItemModel()
        mod.set('guid',require('node-uuid').v1())
        mod.set('title',childview.model.get('title'))
        mod.set('youtrackId',childview.model.get('youtrackId'))
        mod.set('inflowFullPath',childview.model.get('inflowFullPath'))
        mod.set('inflowId',childview.model.get('inflowId'))
        mod.set('inflowTypeId',childview.model.get('inflowTypeId'))
        @showTrackView(@moment())
        @collection.add mod

      currentDayView.on 'childview:editDescription', (childview) =>
        vw = new Show.EditDescriptionView({
          model: childview.model
        })
        onHidden = () =>
          childview.render()
          App.vent.off 'bottom:panel:hidden', onHidden
        App.vent.on 'bottom:panel:hidden', onHidden
        vw.on 'close', () =>
          App.hideBottomPanel()
        App.setBottomPanel(vw)
