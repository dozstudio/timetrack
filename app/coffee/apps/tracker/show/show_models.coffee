App.module "TrackerApp.Show.Models", (Models, App, Backbone, Marionette, $, _)->
  class Models.DayItemModel extends Backbone.Model

    defaults:
      'startDate': new Date()
      'duration': 0
      'title': ''
      'youtrackId': undefined
      'inflowFullPath': 'Inflow ...'
      'isRunning': false
      'taskId': _.uniqueId('task_')

    initialize: ->
      App.log("Initialize DayItemModel")
      @moment = require('moment')
      StopWatch = require('agstopwatch')
      @sw = new StopWatch()
      @timeToAdd = 0
      @timeToInject = 0

    getInflowUpdateable: (()->
      ret = !@get('uploadedInflow') && @get('inflowId') != 0 && @get('inflowId') != undefined && @get('title') != undefined && @get('title') != "" && !@get('pause')
      ret
    ).property('uploadedInflow', 'inflowId', 'title')

    getYoutrackUpdateable: (()->
      ret = !@get('uploadedYoutrack') && @get('youtrackId') != undefined && @get('title') != undefined && @get('title') != "" && !@get('pause')
      ret
    ).property('uploadedYoutrack', 'youtrackId', 'title')

    getUploaded: (()->
      if !@get('uploadedYoutrack') && !@get('uploadedInflow')
        return false
      else if @get('uploadedYoutrack') && !@get('uploadedInflow') && @get('inflowId')
        return false
      else if @get('uploadedInflow') && !@get('uploadedYoutrack') && @get('youtrackId')
        return false
      return true
    ).property('uploadedYoutrack', 'uploadedInflow', 'youtrackId', 'inflowId')

    getEditable: (()->
      !@get('uploadedYoutrack') && !@get('uploadedInflow')
    ).property('uploadedYoutrack', 'uploadedInflow')

    getInflowType: (->
      typeId = @get('inflowTypeId')
      type = App.Inflow.types.findWhere
        id: typeId
      if type
        type.get('name')
      else
        ""
    ).property('inflowTypeId')

    getYoutrackPath: (->
      if @get('pause')
        ''
      else if @get('youtrackId')
        @get('youtrackId')
      else
        'Youtrack ...'
    ).property('youtrackId')

    addDuration: (time)->
      App.log("Adding duration : #{time}")
      @timeToAdd += time
      @set('duration', @get('duration') + time)

    startTimer: =>
      App.log("Starting timer...")
      @sw.start()
      #      @setInactivityTracking()
      if @.get('duration') != null
        @timeToAdd = parseInt(@.get('duration'))

      @listenTo App.vent, "hb:1m", @setElapsedTime
      @set('isRunning', true)

    setElapsedTime: =>
      duration = @sw.elapsed + @timeToAdd
      @set('duration', duration)

    stopTimer: =>
      App.log("Stopping timer...")
      try
        if @sw.running
          @sw.stop()
      catch

      if(@get('isRunning'))
        @stopListening App.vent, "hb:1m", @setElapsedTime
        @set('isRunning', false)

    cancelInflowSelection: ->
      App.log("Cancel inflow selection")
      @set('inflowFullPath', 'Inflow ...')
      @set('inflowId', undefined)
      @set('inflowTypeId', undefined)

    cancelYoutrackSelection: ->
      App.log("Cancel youtrack selection")
      @set('youtrackId', undefined)

    normalize: ()->
      roundMins = App.request 'preference:get', 'RoundTimeMinutes'
      mom = @moment.duration(@get('duration'))
      m = mom.asMinutes()
      val = Math.ceil(m / roundMins) * roundMins
      mom = @moment.duration(val, 'minutes')
      App.log("Normalize task time from #{m} to #{val}")
      @set('duration', mom.asMilliseconds())

  class Models.DayItemCollection extends Backbone.Collection
    model: Models.DayItemModel

  class Models.DayDetails extends Backbone.Model

    initialize: ->
      App.log("Initialize day details")
      @set('items', new Models.DayItemCollection())
      @moment = require "moment"
      @set('dayStart', @moment().format())
      @set('inactivities', new Backbone.Collection())
      @afk = require 'afk'
      #Save on quit
      #TODO: Save on quit
#      win = require('nw.gui').Window.get()
#      win.on 'close', =>
#        @writeFile()
#        win.close(true)

    trackEvents: =>
      @stopListening()
      @listenTo @get('items'), 'change:duration', @sendTimes
      @listenTo @get('items'), 'change:duration', @computeTotalTime
      @listenTo @get('items'), 'remove', @computeTotalTime
      @listenTo @get('items'), 'remove', @writeFile
      @listenTo @get('items'), 'remove', @sendTimes
      @listenTo @get('items'), 'change:uploaded', @computeInflowTotalTime
      @listenTo @get('items'), 'change:duration', @writeFile
      @listenTo @get('items'), 'change:uploadedYoutrack', @writeFile
      @listenTo @get('items'), 'change:uploadedInflow', @writeFile
      @listenTo @get('items'), 'change:title', @writeFile
      @listenTo @get('items'), 'change:inflowId', @writeFile
      @listenTo @get('items'), 'change:youtrackId', @writeFile
      @listenTo @get('items'), 'change:description', @writeFile
      @listenTo @get('items'), 'change:inflowFullPath', @writeFile
      @listenTo @, 'change:dayStart', @sendTimes
      @listenTo App.vent, "hb:minute:changed", @sendTimes

    removeInactivityTracking: ->
      App.log("Remove inactivity tracking")
      if(@listenerId)
        @afk.removeListener(@listenerId)
        @listenerId = undefined

    setInactivityTracking: ()->
      App.log("Set inactivity tracking")
      seconds = (App.request 'preference:get', 'InactivityTimeOutInMinutes') * 60
#      seconds = 10
      @removeInactivityTracking()
      @listenerId = @afk.addListener(seconds, (e)=>
        if(e.status == 'away')
          App.log('User is away')
          itm = @get('items').findWhere
            isRunning: true
          if itm and !itm.get('pause')
            itm.stopTimer()
            durr = itm.get('duration')
            itm.set('duration', @moment.duration(durr).subtract(seconds, 'seconds').asMilliseconds())
          @inactiveDate = @moment().subtract(seconds, 'seconds')
          if itm and itm.get('pause')
            @inactiveDate = undefined
        else if (e.status = 'back')
          App.log("User is back")
          if @inactiveDate
            @backDate = @moment()
            mod = new Backbone.Model
              inactivityId: _.uniqueId('inact_')
              startDate: @inactiveDate
              endDate: @backDate
              duration: @moment(@backDate).diff(@moment(@inactiveDate))
            @get('inactivities').add mod
          @sendTimes()
      )

    loadFile: (date)=>
      @trackEvents()
      @get('items').reset()
      @set('daySelected', date)
      @set('dayStart', @moment().format())

      unless date
        date = new Date()
      @dataPath = App.request 'preference:get', 'DataPath'
      @dataPath = @dataPath.trimEnd('/') #remove pending slash
      @date = @moment(date)
      @filePath = "#{@dataPath}/#{@date.format('YYYY-MM-DD')}.json"
      #load the file
      @fs = require "fs"
      exists = @fs.existsSync @filePath
      if exists
        content = @fs.readFileSync(@filePath, 'utf8')
        if(content)
          dayContent = JSON.parse(content)
          @set('dayStart', dayContent['dayStart'])
          @set('dayEnd', dayContent['dayEnd'])
          @set('timerRunning', dayContent['timerRunning'])
          _.each(dayContent.inactivities, (inact)=>
            inactItem = new Backbone.Model()
            inactItem.set('inactivityId', inact['inactivityId'])
            inactItem.set('startDate', inact['startDate'])
            inactItem.set('endDate', inact['endDate'])
            inactItem.set('duration', inact['duration'])
            @get('inactivities').add inactItem
          )
          _.each(dayContent.items, (item)=>
            bItem = new Models.DayItemModel()
            if(item['taksId'])
              bItem.set('taskId', item['taskId'])
            bItem.set('startDate', item['startDate'])
            bItem.set('endDate', item['endDate'])
            bItem.set('title', item['title'])
            bItem.set('duration', item['duration'])
            bItem.set('description', item['description'])
            bItem.set('uploadedYoutrack', item['uploadedYoutrack'])
            bItem.set('uploadedInflow', item['uploadedInflow'])
            bItem.set('youtrackId', item['youtrackId'])
            bItem.set('inflowId', item['inflowId'])
            bItem.set('inflowTypeId', item['inflowTypeId'])
            bItem.set('pause', item['pause'])
            if item['inflowFullPath']
              bItem.set('inflowFullPath', item['inflowFullPath'])
            bItem.set('isRunning', false)
            @get('items').add(bItem)
          )

      App.log("Loading file '#{@filePath}'")

      @setInactivityTracking()

      @sendTimes()
      @computeTotalTime()
      @computeInflowTotalTime()


    sendTimes: ()=>
      vals = @getTimes()
      @trigger 'times', vals
      @set('dayEnd', new Date())

    clearExtractedTime: ()=>
      App.log("Clear Extracted time")
      @set('extractedTime', undefined)
      @get('items').forEach (i)=>
        i.set('timeToInject': undefined)

    prepareExtractedTime: (timeModel)=>
      App.log("Prepare Extracted time : #{timeModel.get('duration')}")
      @set('extractedTime', timeModel)
      @get('items').forEach (i)=>
        i.set('timeToInject': timeModel)

    deleteInactivity: (inactId)=>


    getTimes: =>
      totalDifference = @moment().diff(@moment(@get('dayStart')))
      trackedTime = 0
      pauseTime = 0
      @get('items').forEach (item)=>
        unless(item.get('pause'))
          trackedTime += item.get('duration')
        else
          pauseTime += item.get('duration')

      total: totalDifference
      trackedTime: trackedTime
      pauseTime: pauseTime
      inactivities: @get('inactivities').toJSON()

    writeFile: ()=>
      toSave =
        items: []
        dayStart: @get('dayStart')
        dayStop: @get('dayStop')
        timerRunning: @get('timerRunning')
        inactivities: []
      @get('items').forEach (m)=>
        toSave.items.push
          taskId: m.get('taskId')
          title: m.get('title')
          youtrackId: m.get('youtrackId')
          inflowId: m.get('inflowId')
          inflowTypeId: m.get('inflowTypeId')
          inflowFullPath: m.get('inflowFullPath')
          uploadedInflow: m.get('uploadedInflow')
          uploadedYoutrack: m.get('uploadedYoutrack')
          description: m.get('description')
          startDate: m.get('startDate')
          endDate: m.get('endDate')
          duration: m.get('duration')
          pause: m.get('pause')
      @get('inactivities').forEach (i)=>
        inactivityId: i.get('inactivityId')
        startDate: i.get('startDate')
        endDate: i.get('endDate')
        duration: i.get('duration')
      #backup current File
      fileContent = JSON.stringify(toSave, null, 2)
      try
        @fs.writeFile(@filePath, fileContent)
        App.log("Writing file #{@filePath}")
      catch
        App.log('An error occured while write the datafile')

    stopAll: =>
      App.log("Stopping all running items")
      @get('items').where(isRunning: true).forEach (model)->
        model.stopTimer()

    normalizeTimes: =>
      App.log("Normalize times")
      @stopAll()
      @get('items').forEach (model)->
        model.normalize()

    computeTotalTime: ->
      dur = 0;
      @get('items').forEach (m)->
        unless m.get('pause')
          dur += m.get('duration')
      dur = @moment.duration(dur)
      @set('totalTime', dur)

    computeInflowTotalTime: ->
      @set('inflowTotalTime', 'Refreshing...')
      $.when(App.request('inflow:select', @get('daySelected'), @get('daySelected')))
      .done (data)=>
        totalMinutes = 0
        data.forEach (d)=>
          totalMinutes += d.get('performed_time')
        mom = require('moment')
        value = mom.duration(totalMinutes, 'minutes')
        @set('inflowTotalTime', value.hours().toDigits(2) + 'h' + value.minutes().toDigits(2))
      .fail =>
        @set('inflowTotalTime', 'Not Connected !')
