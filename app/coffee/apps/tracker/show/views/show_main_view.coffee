App.module "TrackerApp.Show", (Show, App, Backbone, marionette, $, _)->
  class Show.MainView extends Marionette.LayoutView
    template: 'tracker/show/main_view'

    regions:
      dayRegion: '#day-region'
      youtrackRegion: '#youtrack-region'
      inflowRegion: '#inflow-region'
