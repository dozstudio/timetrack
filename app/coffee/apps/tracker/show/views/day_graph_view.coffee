App.module "TrackerApp.Show", (Show, App, Backbone, marionette, $, _)->
  class Show.DayGraphBlock extends Marionette.ItemView
    template: 'tracker/show/day_graph_block'

    events:
      'mousedown': 'onMouseDown'

    onMouseDown: ->
      val =  event.which
      if val == 1
        unless @model.get('type') == 'tracked'
          @trigger 'extract', @model



    onRender: ->
      @$el = @$el.children()
      @$el.unwrap()
      @setElement(@$el)

  class Show.DayGraphView extends Marionette.CompositeView
    template: 'tracker/show/day_graph_view'
    childView: Show.DayGraphBlock
    childViewContainer: '.progress'

    events:
      'mouseover .progress': 'stopRender'
      'mouseout .progress': 'startRender'

    initialize: ->
      @moment = require('moment')

    stopRender: ->
      @noRender = true

    startRender: ->
      @noRender = false

    setGraphValues: (obj)->
      trackedPercentage = Math.ceil((100 / obj.total) * obj.trackedTime)
      pausePercentage = Math.ceil((100 / obj.total) * obj.pauseTime)
      inactiveTotalPercentage = 0
      inactDuration = 0
      @collection = new Backbone.Collection()

      trackedDuration = @moment.duration(obj.trackedTime)
      pauseDuration = @moment.duration(obj.pauseTime)

      @collection.add
        type: 'tracked'
        css: 'progress-bar-tracked'
        title: "Tracked time : #{trackedDuration.hours().toDigits(2)}:#{trackedDuration.minutes().toDigits(2)}"
        percentage: trackedPercentage

      @collection.add
        type: 'pause'
        css: 'progress-bar-pause'
        title: "Pause time: #{pauseDuration.hours().toDigits(2)}:#{pauseDuration.minutes().toDigits(2)}"
        percentage: pausePercentage
        duration: pauseDuration


      _.each(obj.inactivities, (i)=>
        inactPercentage = Math.ceil((100 / obj.total) * i.duration)
        inactiveTotalPercentage += inactPercentage
        inactDuration += i.duration
        alt = !alt
        @collection.add
          type: 'inactive'
          css: "progress-bar-inactive"
          title: "Idle from #{@moment(i.startDate).format('HH:mm')} to #{@moment(i.endDate).format('HH:mm')}"
          percentage: inactPercentage
          inactivityId: i.inactivityId
          duration: +i.endDate -  +i.startDate
      )

      untrackedPercentage = 100 - trackedPercentage - inactiveTotalPercentage - pausePercentage

      untrackedDuration = @moment.duration(obj.total - trackedDuration - inactDuration - pauseDuration)

      @collection.add
        type: 'untracked'
        css: 'progress-bar-untracked'
        title: "Non-tracked time : #{untrackedDuration.hours().toDigits(2)}:#{untrackedDuration.minutes().toDigits(2)}"
        percentage: untrackedPercentage
        duration: untrackedDuration.asMilliseconds()

      unless @noRender
        @render()

      App.log "Setting graph values : tracked[#{obj.trackedTime}] / pause[#{obj.pauseTime}] / Inactivities[#{inactDuration}]  untracked[#{untrackedDuration}] "


    onRender: ->
      @$('[rel="tooltip"]').tooltip()

