App.module "TrackerApp.Show", (Show, App, Backbone, marionette, $, _)->
  class Show.YoutrackItemView extends Marionette.ItemView
    template: 'tracker/show/youtrack_item_view'
    tagName: 'tr'

    events:
      'click #btIdSelected': 'idSelected'

    idSelected: (e)->
      e.preventDefault()
      @trigger 'youtrack:selection',
        id: @model.get('id')
        summary: @model.getFieldValue('summary')

    onRender: ->
      @stickit()

  class Show.YoutrackTableView extends Marionette.CompositeView
    template: 'tracker/show/youtrack_table_view'
    childViewContainer: 'tbody'
    childView: Show.YoutrackItemView

  class Show.YoutrackView extends Marionette.LayoutView
    template: 'tracker/show/youtrack_view'

    ui:
      'search': '#searchInput'
      'projects': '#projects'

    events:
      'keyup #searchInput': 'searchInput'
      'click #btClose': 'close'
      'click #btSearch': 'search'

    bindings:
      '#searchInput': 'searchValue'

    regions:
      resultRegion: '#result-region'

    modelEvents:
      'change:projects': 'render'

    initialize: (options) ->
      @model = new Backbone.Model()
      if(options)
        if(options.search)
          @model.set('searchValue', options.search)
        if(options.lastProject)
          @lastProject = options.lastProject


    onShow: ->
      $.when(App.request 'youtrack:login').done ()=>
        $.when(App.request "youtrack:projects").done (projects)=>
          @model.set('projects', projects.toJSON())
          @render()


    searchInput: (e)->
      if(e.keyCode == 13)
        @search()


    search: ->
      options =
        filter: @model.get('searchValue')
        project: @ui.projects.val()
      @trigger('search', options)

    close: ->
      App.hideBottomPanel()

    onRender: ->
      @stickit()
      if @lastProject
        @$('#projects').val(@lastProject)
