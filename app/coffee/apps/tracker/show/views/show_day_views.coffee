App.module "TrackerApp.Show", (Show, App, Backbone, marionette, $, _)->

  class Show.DayItemView extends Marionette.ItemView
    template: 'tracker/show/day_item_view'
    tagName: 'tr'

    modelEvents:
      'change:isRunning': 'render'
      'change:uploaded': 'render'
      'change:uploadedYoutrack': 'render'
      'change:uploadedInflow': 'render'
      'change:InflowId': 'render'
      'change:youtrackId': 'render'
      'change:inflow_updateable': 'render'
      'change:youtrack_updateable': 'render'
      'change:timeToInject': 'render'

    triggers:
      'click #btPlay': 'start'
      'click #btStop': 'stop'
      'click #btRemove': 'remove'
      'click #btUploadInflow': 'inflow:upload'
      'click #btUploadYoutrack': 'youtrack:upload'
      'click #btInjectTime': 'injectTime'
      'click #btDuplicateToday': 'duplicateToday'
      'click #btEditDescription': 'editDescription'

    ui:
      'inflowlink': '#inflowSearch'

    events:
      'click #btEditTime': 'editTime'
      'click #inflowSearch': 'inflowSearch'
      'click #youtrackSearch': 'youtrackSearch'
      'click #youtrackRemove': 'cancelYoutrackSelection'
      'click #inflowRemove': 'cancelInflowSelection'
      'blur #durationEditor': 'stopEditTime'
      'keydown #durationEditor': 'keyUpDurationEditor'

    bindings:
      '#title':
        observe: 'title'
        events: ['blur']
      '#youtrackSearch': 'youtrackPath'
      '#inflowSearch': 'inflowFullPath'
      '#duration':
        observe: 'duration'
        onGet: (value, option)->
          if(value)
            mom = require "moment"
            value = mom.duration(value)
            value.hours().toDigits(2) + 'h' + value.minutes().toDigits(2)
          else
            "00h00"

      '#durationEditor':
        observe: 'duration'
        onGet: (value, option)->
          if(value)
            mom = require "moment"
            value = mom.duration(value)
            value.hours().toDigits(2) + 'h' + value.minutes().toDigits(2)
          else
            "00h00"

        onSet: (val, option)=>
          mom = require "moment"
          vals = val.split('h')
          if vals.length > 1
            hours = parseInt(vals[0])
            minutes = parseInt(vals[1])
          else
            hours = 0
            minutes = parseInt(vals[0])
          durr = mom.duration((hours * 60) + minutes, 'minutes')
          durr.asMilliseconds()

    inflowSearch: (e)->
      e.preventDefault()
      unless @model.get 'uploadedInflow'
        @trigger 'inflow:search',
          inflowId: @model.get('inflowId')
          typeId: @model.get('inflowTypeId')

    youtrackSearch: (e)->
      e.preventDefault()
      unless @model.get 'uploadedYoutrack'
        @trigger 'youtrack:search'


    keyUpDurationEditor: (e)=>
      if(e.keyCode == 13)
        @stopEditTime()

    editTime: ->
      @model.stopTimer()
      @$('#duration').hide()
      @$('#durationEditor').show()
      @$('#durationEditor').focus()
      @$('#btPlay').hide()
      @$("#btEditTime").hide()

    stopEditTime: ->
      @$('#durationEditor').hide()
      @$('#duration').show()
      @$('#btPlay').show()
      @$("#btEditTime").show()

    cancelInflowSelection: ->
      @model.cancelInflowSelection()

    cancelYoutrackSelection: ->
      @model.cancelYoutrackSelection()

    focusOnTitle: ->
      @$('#title').focus()

    onRender: ->
      @stickit()
      @$('[rel="tooltip"]').tooltip()
      if(@model.get('isRunning'))
        @$el.addClass('running')
      else
        @$el.removeClass('running')

      $.mask.definitions['6'] = "[0-5]";
      $.mask.definitions['3'] = "[0-2]";
      @$('#durationEditor').mask('39h69')

    setInflowSelection: (data)->
      @model.set('inflowId', data.InflowId)
      @model.set('inflowFullPath', data.FullProjectName)
      @model.set('inflowTypeId', data.TypeId)

    setYoutrackSelection: (data)=>
      obj = {}
      obj.youtrackId = data.id

      preparedTitle = "#{data.id} - #{data.summary}"
      if @model.get('title') == ""
        obj.title = preparedTitle
      else if confirm "Do you want to change task title ?"
        obj.title = preparedTitle

      @model.set(obj)

    onDomRefresh: ()->
      @$('#title').focus()

  class Show.CurrentDayView extends Marionette.CompositeView
    template: 'tracker/show/current_day_view'
    childViewContainer: 'tbody'
    childView: Show.DayItemView

    triggers:
      'click #btAddToday': 'add'
      'click #btAddAndPlayToday': 'addAndStart'
      'click #btNormaliseTimes': 'normalizeTimes'
      'click #btPrevDay': 'goToPreviousDay'
      'click #btNextDay': 'goToNextDay'
      'click #removeTaskSelection': 'removeTaskSelection'
      'click #btAddPause': 'addPause'
      'click #btRefreshInflowTime': 'refreshInflowTime'

    events:
      'click #dateLabel': 'showLabelDateSelector'
      'click #btUploadInflow': 'uploadAll'
#      'click #btStart': 'startDay'
      'blur #startDayEditor': 'stopEditStartDay'
      'keydown #startDayEditor': 'keyUpDayEditor'
      'click #btEditStartDay': 'editStartOfTheDay'

    modelEvents:
      'change:daySelected': 'dayChanged'

    bindings:
      '#lblTotalTimeInflow': 'inflowTotalTime'
      '#selectTaskLabel':
        observe: 'extractedTime'
        onGet: 'onGetExtractedTimeValue'

      '#lblTotalDurationToday':
        observe: 'totalTime'
        onGet: (value, option)->
          if value
            mom = require "moment"
            value = mom.duration(value)
            value.hours().toDigits(2) + 'h' + value.minutes().toDigits(2)
          else
            ""


      '#lblDurationLeftToday':
        observe: 'totalTime'
        onGet: (value, option)->
          if value
            mom = require "moment"
            value = mom.duration(value)
            now = mom()
            dur = mom.duration(8, 'hours').subtract(value)
            ms = dur.asMilliseconds()
            minus = false
            if(ms < 0)
              minus = true
              ms = 0 - ms
            dur = mom.duration(ms)
            time = dur.hours().toDigits(2) + 'h' + dur.minutes().toDigits(2)
            if minus
              "You can leave office now !!"
            else
              timeToLeave = now.add(dur.asMinutes(), 'minutes')
              time+" left - Leave office at #{timeToLeave.hours().toDigits(2)}h#{timeToLeave.minutes().toDigits(2)}"
          else
            ""

      '#dateInput':
        observe: 'daySelected'
        onSet: (value, option)->
          mom = require "moment"
          if value == ""
            value = @moment().format('DD/MM/YYYY')

          mom(value, 'DD/MM/YYYY').toDate()
        onGet: (value, option)->
          mom = require "moment"
          value = mom(value)
          value.format('DD/MM/YYYY')

      '#dateLabel':
        observe: 'daySelected'
        onGet: (value, option)->
          mom = require "moment"
          value = mom(value)
          value.format('DD/MM/YYYY')

      '#startDay':
        observe: 'dayStart'
        onGet: (value)->
          mom = require 'moment'
          value = mom(value)
          value.format('HH:mm')

      '#endDay':
        observe: 'dayEnd'
        onGet: (value)->
          mom = require 'moment'
          value = mom(value)
          value.format('HH:mm')

      '#startDayEditor':
        observe: 'dayStart'
        onGet: (value, option)=>
          mom = require "moment"
          value = mom(value)
          value.format('HH:mm')

        onSet: (val, option)=>
          mom = require "moment"
          values = val.split(':')
          if(values.length == 2)
            day = mom()
            day.set('hours', values[0])
            day.set('minutes', values[1])
            day.set('seconds', 0)

            day.format()

    onGetExtractedTimeValue: (value, options)->
      if value
        mom = require('moment')
        durr = mom.duration(value.get('duration'))
        val = durr.hours().toDigits(2) + 'h' + durr.minutes().toDigits(2) + 'm' + durr.seconds().toDigits(2) + 's'
        @$('#removeTaskSelection').show()
        "Select task to inject duration of '#{val}'"
      else
        @$('#removeTaskSelection').hide()
        ""


    initialize: (options)->
      @model = (options && options.model) || new Backbone.Model()
      @collection = (options && options.collection) || new Backbone.Collection()
      regionManager = new Marionette.RegionManager()
      @graphRegion = regionManager.addRegion('graphRegion', '#graph-region')
      @moment = require('moment')

    editStartOfTheDay: ->
      @$('#startDay').hide()
      @$('#startDayEditor').show()
      @$('#startDayEditor').focus()

    stopEditStartDay: ->
      @$('#startDayEditor').hide()
      @$('#startDay').show()

    keyUpDayEditor: (e)->
      if(e.keyCode == 13)
        @stopEditStartDay()

    dayChanged: (mod, mom)->
      @trigger 'dayChanged', mom

    uploadAll: ->
      if confirm('Upload all tasks to inflow and youtrack ?')
        @children.each (v)->
          if v.model.get('inflow_updateable')
            v.trigger 'inflow:upload'
          if v.model.get('youtrack_updateable')
            v.trigger 'youtrack:upload'

    onAddChild: (childview)->
      childview.focusOnTitle()

    showLabelDateSelector: ->
      @$('#dateInput').datepicker('show')

    onRender: ()->
      @stickit()

      if(@moment(@model.get('daySelected')).format('DD/MM/YYYY') == @moment().format('DD/MM/YYYY'))
        @$('#btNextDay').hide()
      else
        @$('#btNextDay').show()

      $.mask.definitions['6'] = "[0-5]";
      $.mask.definitions['3'] = "[0-2]";
      @$('#startDayEditor').mask('39:69')
      @$('[data-toggle="tooltip"]').tooltip()
      @$('#dateInput').datepicker
        language:'fr'
        todayBtn: "linked"
        orientation: "top left"
        forceParse: false
        autoclose: true
