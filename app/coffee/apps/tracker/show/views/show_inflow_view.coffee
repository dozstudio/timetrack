App.module "TrackerApp.Show", (Show, App, Backbone, marionette, $, _)->
  class Show.InflowView extends Marionette.ItemView
    template: 'tracker/show/inflow_view'

    ui:
      'tree': '#treeGrid'
      'typeSelector': '#typeSelector'
      'search': '#searchInput'

    events:
      'keyup #searchInput': 'search'
      'keydown #treeGrid': 'keyDownTreeGrid'
      'click #btClose': 'close'
      'change #typeSelector': 'typeSelectorChanged'

    initialize: (options)->
      @inflowId = options.inflowId
      @typeId = options.typeId
      @data = []
      @to = false
      App.vent.on 'bottom:panel:shown', =>
        $(@ui.search).focus()
      unless App.Inflow
        $.when(
          App.request("inflow:types"),
          App.request("inflow:tree")
        ).done (types, tree)=>
          App.Inflow =
            types: types
            tree: tree
          @parseData(App.Inflow.tree, @data)
      else
        @parseData(App.Inflow.tree, @data)


    parseData: (collection, root, parentName)->
      collection.each (project)=>
        if parentName
          FullName = "#{parentName} > #{project.get('ProjectName')}"
        else
          FullName = "#{project.get('ProjectName')}"
        obj =
          id: project.get('InflowId')
          text: project.get('ProjectName')
          fullName: FullName
          children: []
          state:
            selected: project.get('InflowId') == @inflowId

        @parseData(project.get('Children'), obj.children, obj.fullName)
        root.push obj

    keyDownTreeGrid: (e)->
      if(e.keyCode == 13)
        App.hideBottomPanel()

    close: ->
      App.hideBottomPanel()

    search: (e)->
      if(e.keyCode == 13)
        if(@to)
          clearTimeout(@to)
        @to = setTimeout(=>
          val = @ui.search.val()
          @ui.tree.jstree(true).search(val)
        , 500)

    serializeData: ->
      types: App.Inflow.types.toJSON()

    typeSelectorChanged: ->
      @triggerNodeData()

    triggerNodeData: ()->
      data = @ui.tree.jstree('get_selected', full: true)
      if data.length > 0
        @trigger 'inflow:selected',
          InflowId: data[0].original.id
          ProjectName: data[0].original.text
          FullProjectName: data[0].original.fullName
          TypeId: @$('#typeSelector').val()

    onRender: ->
      $.jstree.defaults.search.show_only_matches = true
      $.jstree.defaults.core.multiple = false
      @ui.tree.jstree
        core:
          data: @data
        plugins: ["sort", "search"]

      @ui.tree.on 'changed.jstree', (e, data)=>
        @triggerNodeData()

      $(@ui.typeSelector).val(@typeId)

