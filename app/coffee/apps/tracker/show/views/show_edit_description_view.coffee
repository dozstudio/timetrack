App.module "TrackerApp.Show", (Show, App, Backbone, marionette, $, _)->
  class Show.EditDescriptionView extends Marionette.ItemView
    template: 'tracker/show/edit_description_view'

    triggers:
      'click #btClose': 'close'

    bindings:
      '#description': 'description'

    onRender: () =>
      @stickit()
