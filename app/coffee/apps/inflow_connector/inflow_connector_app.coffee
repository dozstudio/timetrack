App.module "InflowConnectorApp", (InflowConnectorApp, App, Backbone, Marionette, $, _)->

  InflowConnectorApp.on "start", ->
    @ctl = new InflowConnectorApp.Controller()


  App.reqres.setHandler "inflow:types", ()=>
    @ctl.getInflowTypes()

  App.reqres.setHandler "inflow:tree", ()=>
    @ctl.getInflowTree()

  App.reqres.setHandler "inflow:add:prest", (details)=>
    @ctl.putInflowTask(details)

  App.reqres.setHandler "inflow:select", (startDate, endDate, user)=>
    @ctl.selectInflowData(startDate, endDate,user)

  App.reqres.setHandler "inflow:select:projectRoots", (ids)=>
    @ctl.getInflowProjectRoots(ids)

  App.reqres.setHandler "inflow:select:projectChildrens", (id)=>
    @ctl.getInflowProjectWithChildrenRoots(id)

  App.reqres.setHandler "inflow:select:allperfs", (id,startDate,endDate,template)=>
    @ctl.getInflowPerfsForProject(id,startDate,endDate,template)

  App.reqres.setHandler "inflow:select:solvers", ()=>
    @ctl.getInflowPerformers()
