App.module "InflowConnectorApp", (InflowConnectorApp, App, Backbone, Marionette, $, _)->
  class InflowConnectorApp.Controller extends Marionette.Controller

    initialize: ()->
      @mom = require 'moment'

    selectInflowData: (date, dateEnd, user)->
      deferred = $.Deferred()

      unless date
        date = new Date()
      unless dateEnd
        dateEnd = new Date()
      if(user == undefined || user == "")
        user = App.request 'preference:get', 'InflowUser'
      pg = require 'pg'
      conString = App.request 'preference:get', 'InflowDatabaseUrl'
      moment = require('moment')
      formattedStartDate = moment(date).format('YYYY-MM-DD')
      formattedEndDate = moment(dateEnd).format('YYYY-MM-DD')
      App.log("Select inflow date on dates : #{formattedStartDate} to #{formattedEndDate}")
      query = "SELECT prob.solver,
       To_char(prob.creation_date, 'YYYY-MM-DD HH24:MI:SS') AS \"creation_date\",
       prob.performed_time,
       prob.title,
       prob.app_func_seqid,
       tmp.NAME,
       t.code,
       ( WITH recursive parents(seqid, parent_app_func_seqid, NAME) AS
       (
              SELECT seqid,
                     parent_app_func_seqid,
                     NAME
              FROM   application_function
              WHERE  seqid=prob.app_func_seqid
              UNION ALL
              SELECT t.seqid,
                     t.parent_app_func_seqid,
                     t.NAME
              FROM   parents p
              JOIN   application_function t
              ON     p.parent_app_func_seqid = t.seqid )SELECT NAME
	      FROM   parents
	      WHERE  parent_app_func_seqid IS NULL ) AS \"root_project\",
	      ( WITH recursive parents(seqid, parent_app_func_seqid, NAME) AS
       (
              SELECT seqid,
                     parent_app_func_seqid,
                     NAME
              FROM   application_function
              WHERE  seqid=prob.app_func_seqid
              UNION ALL
              SELECT t.seqid,
                     t.parent_app_func_seqid,
                     t.NAME
              FROM   parents p
              JOIN   application_function t
              ON     p.parent_app_func_seqid = t.seqid )SELECT seqid
	      FROM   parents
	      WHERE  parent_app_func_seqid IS NULL ) AS \"root_id\",
	      af.Name as \"node_name\"
FROM   PUBLIC.problem prob,
       PUBLIC.type t,
       PUBLIC.template tmp,
       PUBLIC.application_function af
WHERE  prob.type_seqid = t.seqid
AND    t.template_id = tmp.seqid
AND    prob.app_func_seqid = af.seqid
AND    tmp.NAME = 'PERF'
AND    lower(prob.solver) = lower('#{user}')
AND    prob.creation_date::date >='#{formattedStartDate}'
AND    prob.creation_date::date <='#{formattedEndDate}'"

      pg.connect conString, (err, client, done)->
        if(err)
          App.log("Unable to connect to inflow database...")
          deferred.reject()
        if client
          client.query(query, (err, results)->
            done()
            if(err)
              App.log("Unable to get inflow times from database...")
              deferred.reject()
            else
              App.log("#{results.rows.length} record(s) returned from inflow database")
              perfs = new InflowConnectorApp.InflowPerfCollection(results.rows)

              deferred.resolve(perfs)
          )

      deferred.promise()


    getInflowPerfsForProject: (id, dateStart, dateEnd,template)=>
      deferred = $.Deferred()
      if(template == undefined)
        template = "PERF"
      $.when(@getInflowProjectWithChildrenRoots(id))
      .then (rootsRecords)=>
        joinSeqid = rootsRecords.map((el)->
          el.get('seqid')
        ).join()
        moment = require('moment')
        formattedStartDate = moment(dateStart).format('YYYY-MM-DD')
        formattedEndDate = moment(dateEnd).format('YYYY-MM-DD')
        query = "
          SELECT upper(prob.solver) as \"solver\",
       To_char(prob.creation_date, 'YYYY-MM-DD HH24:MI:SS') AS \"creation_date\",
       prob.performed_time,
       prob.title,
       prob.app_func_seqid,
       tmp.NAME,
       t.code,
       ( WITH recursive parents(seqid, parent_app_func_seqid, NAME) AS
       (
              SELECT seqid,
                     parent_app_func_seqid,
                     NAME
              FROM   application_function
              WHERE  seqid=prob.app_func_seqid
              UNION ALL
              SELECT t.seqid,
                     t.parent_app_func_seqid,
                     t.NAME
              FROM   parents p
              JOIN   application_function t
              ON     p.parent_app_func_seqid = t.seqid )SELECT NAME
FROM   parents
WHERE  parent_app_func_seqid IS NULL ) AS \"root_project\",
       ( WITH recursive parents(seqid, parent_app_func_seqid, NAME) AS
       (
              SELECT seqid,
                     parent_app_func_seqid,
                     NAME
              FROM   application_function
              WHERE  seqid=prob.app_func_seqid
              UNION ALL
              SELECT t.seqid,
                     t.parent_app_func_seqid,
                     t.NAME
              FROM   parents p
              JOIN   application_function t
              ON     p.parent_app_func_seqid = t.seqid )SELECT seqid
FROM   parents
WHERE  parent_app_func_seqid IS NULL ) AS \"root_id\"
FROM   PUBLIC.problem prob,
       PUBLIC.type t,
       PUBLIC.template tmp
WHERE  prob.type_seqid = t.seqid
AND    t.template_id = tmp.seqid
AND    prob.app_func_seqid in (#{joinSeqid})
AND    tmp.NAME = '#{template}'
AND    prob.creation_date::date >='#{formattedStartDate}'
AND    prob.creation_date::date <='#{formattedEndDate}'"
        $.when(@queryDatabase(query))
        .then((data)=>
          deferred.resolve(data)
        ).fail ()=>
          deferred.reject()
      deferred.promise()

    queryDatabase: (query)=>
      deferred = $.Deferred()
      pg = require 'pg'
      conString = App.request 'preference:get', 'InflowDatabaseUrl'
      pg.connect conString, (err, client, done)->
        if(err)
          App.log("Unable to connect to inflow database...")
          deferred.reject()
        if client
          client.query(query, (err, results)->
            done()
            if(err)
              App.log("Unable to get data from database..."+err)
              deferred.reject()
            else
              App.log("#{results.rows.length} record(s) returned from inflow database")
              deferred.resolve(new Backbone.Collection(results.rows))
          )
      deferred.promise()


    getInflowProjectWithChildrenRoots: (id)=>
      unless(Array.isArray(id))
        id = [id]
      App.log("Select inflow children roots for id #{id}")
      query = "
        WITH RECURSIVE parents(seqid, parent_app_func_seqid, name)
        AS(
          SELECT seqid, parent_app_func_seqid, name
          from application_function
          where seqid in (#{id})

          UNION ALL

          SELECT t.seqid, t.parent_app_func_seqid,t.name
          FROM parents p
          JOIN application_function t
          on p.seqid = t.parent_app_func_seqid
        )
        select * from parents"
      @queryDatabase(query)

    getInflowPerformers: ()=>
      App.log("Select inflow performers")
      query = "
        select distinct upper(solver) as \"solver\" from problem
          where solver <> ''
          order by 1 asc"
      @queryDatabase(query)

    getInflowProjectRoots: (ids)=>
      App.log("Select project roots")
      query = "
        WITH RECURSIVE parents(seqid, parent_app_func_seqid, name)
        AS(
          SELECT seqid, parent_app_func_seqid, name
          from application_function
          where seqid=#{ids.join()}

          UNION ALL

          SELECT t.seqid, t.parent_app_func_seqid,t.name
          FROM parents p
          JOIN application_function t
          on p.parent_app_func_seqid = t.seqid
        )
        select * from parents
        where parent_app_func_seqid is null "
      @queryDatabase(query)


    getInflowTree: ()=>
      App.log("Getting inflow tree...")

      fileName = App.Utils.getUserHome() + "/Documents/TimeTracker/inflowTree.json"
      deferred = $.Deferred()
      fs = require('fs')
      unless App.Inflow
        App.Inflow = {}

      exists = fs.exists fileName, (exists)=>
        if(exists)
          #load file
          fs.readFile fileName, 'utf8', (err, fileContent)=>
            unless err
              file = JSON.parse(fileContent)
              App.Inflow.tree = @parseJsonTree(file)
              App.log("Reading from local file !")
              deferred.resolve(App.Inflow.tree)


      inflowUrl = App.request 'preference:get', 'InflowUrl'
      inflowUser = App.request 'preference:get', 'InflowUser'
      inflowPassword = App.request 'preference:get', 'InflowPassword'

      inflowUrl = inflowUrl.trimEnd('/')

      $.ajax
        url: "#{inflowUrl}/tree"
        dataType: 'xml'
        headers:
          "Authorization": "Basic " + btoa(inflowUser + ":" + inflowPassword)

        success: (data)=>
          tree = @parseTreeResults($(data).find('root'))
          App.Inflow.tree = tree
          fs.writeFile fileName, JSON.stringify(tree, null, 2), 'utf8'
          App.log("Local file updated!")
          deferred.resolve(tree)

        error: (data)->
          deferred.fail()

      deferred.promise()

    parseJsonTree: (file)=>
      coll = new InflowConnectorApp.InflowNodeCollection()
      _.each file, (child)=>
        obj = new InflowConnectorApp.InflowNode()
        obj.set('ProjectName', child['ProjectName'])
        obj.set('InflowId', child['InflowId'])
        obj.set('Children', @parseJsonTree(child['Children']))
        coll.add (obj)
      coll

    parseTreeResults: (node)=>
      coll = new InflowConnectorApp.InflowNodeCollection()
      _.each node.children(), (child)=>
        obj = new InflowConnectorApp.InflowNode()
        obj.set('ProjectName', $(child).attr('name'))
        obj.set('InflowId', $(child).attr('id'))
        obj.set('Children', @parseTreeResults($(child)))
        coll.add (obj)
      coll



    getInflowTypes: ()->
      App.log("Getting inflow types")

      fileName = App.Utils.getUserHome() + "/Documents/TimeTracker/inflowTypes.json"
      deferred = $.Deferred()
      fs = require('fs')
      unless App.Inflow
        App.Inflow = {}
      exists = fs.exists fileName, (exists)=>
        if(exists)
          #load file
          fs.readFile fileName, 'utf8', (err, fileContent)=>
            unless err
              App.Inflow.types = new Backbone.Collection(JSON.parse(fileContent))
              App.log("Reading local file")
              deferred.resolve(App.Inflow.types)

        inflowUrl = App.request 'preference:get', 'InflowUrl'
        inflowUser = App.request 'preference:get', 'InflowUser'
        inflowPassword = App.request 'preference:get', 'InflowPassword'

        inflowUrl = inflowUrl.trimEnd('/')
        coll = new InflowConnectorApp.InflowTypes

        $.ajax
          url: "#{inflowUrl}/types/perf"
          headers:
            "Authorization": "Basic " + btoa(inflowUser + ":" + inflowPassword)
          dataType: 'xml'
          success: (data)->
            types = $(data).find('type')
            _.each(types, (t)=>
              coll.add
                id: parseInt $(t).attr('id')
                name: $(t).attr('name')
            )
            App.Inflow.types = coll
            App.log("Writing local file")

            fs.writeFile fileName, JSON.stringify(coll, null, 2), 'utf8'
            deferred.resolve(coll)
          error: (data)->
            deferred.fail()

      deferred.promise()

    putInflowTask: (details)->
      inflowUrl = App.request 'preference:get', 'InflowUrl'
      inflowUser = App.request 'preference:get', 'InflowUser'
      inflowPassword = App.request 'preference:get', 'InflowPassword'
      inflowUrl = inflowUrl.trimEnd('/')
      deferred = $.Deferred()
      App.log("Sending inflow task : #{details.title} - #{details.applicationId} - #{details.minutes} minutes")

      #dd-MM-yyyy HH:mm:ss
      $.ajax
        url: "#{inflowUrl}/perfs"
        type: 'POST'
        data:
          applicationId: details.applicationId
          date: @mom(details.date).format('DD-MM-YYYY HH:mm:ss')
          minutes: parseInt(details.minutes)
          title: details.title
          description: details.description || ''
          comment: details.comment
          typeId: details.typeId
          chargeable: details.chargeable
        headers:
          "Authorization": "Basic " + btoa(inflowUser + ":" + inflowPassword)
        contentType: 'application/x-www-form-urlencoded'
        dataType: 'text'
      .done (data, status)->
        deferred.resolve()
        App.notify 'Inflow upload success'
      .fail (jqxhr, status, errorThrown)->
        deferred.fail()
        App.notify 'Inflow upload ERROR'


      deferred.promise()
