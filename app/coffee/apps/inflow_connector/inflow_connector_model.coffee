App.module "InflowConnectorApp", (InflowConnectorApp, App, Backbone, Marionette, $, _)->
  class InflowConnectorApp.InflowType extends Backbone.Model

  class InflowConnectorApp.InflowTypes extends Backbone.Collection
    model: InflowConnectorApp.InflowType
    comparator: 'id'

  class InflowConnectorApp.InflowNode extends Backbone.Model

  class InflowConnectorApp.InflowNodeCollection extends Backbone.Collection
    model: InflowConnectorApp.InflowNode

    findRootNode:(id)->
      nodeArray = @toArray()
      id = id.toString()
      for node in nodeArray
        if(node.get('InflowId') == id)
          #found
          return node.get('ProjectName')
        else if node.get('Children')
          childFoundVal = node.get('Children').findRootNode(id)
          if(childFoundVal)
            return node.get('ProjectName')
        else
          return undefined

  class InflowConnectorApp.InflowPerf extends Backbone.Model

    constructor: ()->
      moment = require('moment')
      Backbone.Model.apply(@,arguments)

      #convert date
      date = @get('creation_date')
      date = moment(date,"YYYY-MM-DD HH:mm:ss");
      @set('date',date)
      @set('date_s',@get('date').format('DD-MM-YYYY'))

      #set root node
      ###@set('root_project',App.Inflow.tree.findRootNode(@get('app_func_seqid')))###


  class InflowConnectorApp.InflowPerfCollection extends Backbone.Collection
    model: InflowConnectorApp.InflowPerf



