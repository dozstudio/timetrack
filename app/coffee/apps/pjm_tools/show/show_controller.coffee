App.module "PjmToolsApp.Show", (Show, App, Backbone, Marionette, $, _)->

  class Show.Controller extends Marionette.Controller

    show: ->
      App.vent.trigger 'select:link', 'pjmtools:show'

      mainView = new Show.MainView()
      App.setMainView(mainView)

      selectorView = new Show.SelectorView()
      resultView = new Show.ResultView()
      resultView.model = new Backbone.Model()
      mainView.resultRegion.show(resultView)
      $.when(App.request("inflow:select:solvers"))
        .then (data) ->
          selectorView.model = new Backbone.Model
            solvers: data.toJSON()
            projects: _.sortBy(App.Inflow.tree.toJSON(), 'ProjectName')

          mainView.selectorRegion.show(selectorView)

          selectorView.on 'search', (details)=>
            resultView.resultInnerRegion.empty()

            #save Preferences
            App.request 'preference:set',
              name: 'ProjectsPmSelection'
              value: details.projectId
            App.request 'preference:set',
              name: 'TeamUsers',
              value: details.solvers

            resultInnerView = new Show.ResultContentView
              model: new Show.ViewModel.InflowTimeResultModel()
            resultView.showLoading()
            $.when(
              App.request("inflow:select:allperfs",details.projectId,details.startDate,details.endDate,'PERF'),
              App.request("inflow:select:allperfs",details.projectId,details.startDate,details.endDate,'PERF-TRACE'))
            .then (dataPerf, dataPerfTrace)=>
              resultView.hideLoading()
              resultInnerView.model.setInflowData(dataPerf,dataPerfTrace,details.solvers)
              resultView.resultInnerRegion.show(resultInnerView)

          projectsSelected = App.request 'preference:get','ProjectsPmSelection'
          selectorView.setProjectSelection(projectsSelected)
          solverSelected = App.request 'preference:get','TeamUsers'
          selectorView.setSolverSlection(solverSelected)
          selectorView.setThisMonth()
          selectorView.triggerSearch()

        .fail () ->
          resultView.setErrorMessage("Inflow not accessible")
