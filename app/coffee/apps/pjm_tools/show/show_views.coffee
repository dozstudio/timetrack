App.module "PjmToolsApp.Show", (Show, App, Backbone, Marionette, $, _)->

  class Show.MainView extends Marionette.LayoutView
    template: 'pjm_tools/show/main'

    regions:
      selectorRegion:'#selectorRegion'
      resultRegion:'#resultRegion'

  class Show.SelectorView extends Marionette.ItemView
    template: 'pjm_tools/show/selector'

    initialize:->
      @model = new Show.ViewModel.SelectorViewModel()
      @moment = require('moment')

    bindings:
      '#cbProject':'projectSelected'
      '#startDate':
        observe:'startDate'
      '#endDate':
        observe:'endDate'

    events:
      'click #btThisMonth':'setThisMonth'
      'click #btThisWeek':'setThisWeek'
      'click #btThisYear':'setThisYear'
      'click #btSearch': 'triggerSearch'
      'click #goToPrevious' : 'goToPrevious'
      'click #goToNext' : 'goToNext'

    triggerSearch: =>
      startDate = @moment(@$("#startDate").val(),"DD/MM/YYYY")
      endDate = @moment(@$("#endDate").val(),"DD/MM/YYYY")
      projectIds = @$("#cbProject").val()
      if(projectIds != null)
        @trigger 'search',
          projectId: @$("#cbProject").val()
          solvers: @$("#cbSolvers").val()
          startDate:  startDate
          endDate: endDate

    setProjectSelection: (projects)=>
      @$('#cbProject').multiselect('select',projects)

    setSolverSlection: (solvers)=>
      @$('#cbSolvers').multiselect('select',solvers)

    goToPrevious:->
      switch @type
        when "MONTHLY"
          startDate = @moment(@model.get('startDate'),'DD/MM/YYYY').startOf('month').subtract(1,'months').format('DD/MM/YYYY')
          endDate = @moment(startDate,'DD/MM/YYYY').endOf('month').format('DD/MM/YYYY')
        when "WEEKLY"
          startDate = @moment(@model.get('startDate'),'DD/MM/YYYY').startOf('week').subtract(1,'weeks').format('DD/MM/YYYY')
          endDate = @moment(startDate,'DD/MM/YYYY').endOf('week').format('DD/MM/YYYY')
        when "YEARLY"
          startDate = @moment(@model.get('startDate'),'DD/MM/YYYY').startOf('year').subtract(1,'years').format('DD/MM/YYYY')
          endDate = @moment(startDate,'DD/MM/YYYY').endOf('year').format('DD/MM/YYYY')

      @model.set('startDate',startDate)
      @model.set('endDate',endDate)
      @triggerSearch()

    goToNext:->
      switch @type
        when "MONTHLY"
          startDate = @moment(@model.get('startDate'),'DD/MM/YYYY').startOf('month').add(1,'months').format('DD/MM/YYYY')
          endDate = @moment(startDate,'DD/MM/YYYY').endOf('month').format('DD/MM/YYYY')
        when "WEEKLY"
          startDate = @moment(@model.get('startDate'),'DD/MM/YYYY').startOf('week').add(1,'weeks').format('DD/MM/YYYY')
          endDate = @moment(startDate,'DD/MM/YYYY').endOf('week').format('DD/MM/YYYY')
        when "YEARLY"
          startDate = @moment(@model.get('startDate'),'DD/MM/YYYY').startOf('year').add(1,'years').format('DD/MM/YYYY')
          endDate = @moment(startDate,'DD/MM/YYYY').endOf('year').format('DD/MM/YYYY')

      @model.set('startDate',startDate)
      @model.set('endDate',endDate)
      @triggerSearch()

    clearPills: ->
      @$('#btThisMonth').removeClass("active")
      @$('#btThisWeek').removeClass("active")
      @$('#btThisYear').removeClass("active")

    setThisMonth: ->
      @type="MONTHLY"
      @clearPills()
      @$('#btThisMonth').addClass("active")
      startDate = @moment().startOf('month').format('DD/MM/YYYY')
      endDate = @moment().endOf('month').format('DD/MM/YYYY')
      @model.set('startDate',startDate)
      @model.set('endDate',endDate)
      @triggerSearch()

    setThisWeek: ->
      @type="WEEKLY"
      @clearPills()
      @$('#btThisWeek').addClass("active")
      startDate = @moment().startOf('week').format('DD/MM/YYYY')
      endDate = @moment().endOf('week').format('DD/MM/YYYY')
      @model.set('startDate',startDate)
      @model.set('endDate',endDate)
      @triggerSearch()

    setThisYear: ->
      @type="YEARLY"
      @clearPills()
      @$('#btThisYear').addClass("active")
      startDate = @moment().startOf('year').format('DD/MM/YYYY')
      endDate = @moment().endOf('year').format('DD/MM/YYYY')
      @model.set('startDate',startDate)
      @model.set('endDate',endDate)
      @triggerSearch()

    # serializeData:->
    #   projects: @model.get('projects')
    #   solvers: @model.get('solvers')
    #   startDate: @moment().startOf('month').format('YYYY-MM-DD')
    #   endDate: @moment().endOf('month').format('YYYY-MM-DD')

    onRender:->
      @stickit()
      @$('#cbProject').multiselect(
        enableFiltering: true
        buttonClass: 'btn btn-primary'
        includeSelectAllOption: true
        enableCaseInsensitiveFiltering: true
        buttonText: (options,select)->
          if options.length == 0
            return 'No projects'
          else return "#{options.length} projects"

      )
      @$('#cbSolvers').multiselect(
        enableFiltering: true
        buttonClass: 'btn btn-primary'
        includeSelectAllOption: true
        enableCaseInsensitiveFiltering: true
        buttonText: (options,select)->
          if options.length == 0
            return 'No team'
          else return "#{options.length} users"

      )

      @$('#startDate').datepicker
        language:'fr'
        todayBtn: "linked"
        forceParse: false
        autoclose: true

      @$('#endDate').datepicker
        language:'fr'
        todayBtn: "linked"
        forceParse: false
        autoclose: true

  class Show.ResultContentView extends Marionette.ItemView
    template: 'pjm_tools/show/resultContent'

    serializeData: =>
      data: @model.get('data')

  class Show.ResultView extends Marionette.LayoutView
    template: 'pjm_tools/show/result'

    modelEvents:
      'change errorMessage': 'render'

    regions:
      'resultInnerRegion':'#resultContainer'

    setErrorMessage: (message)->
      @model.set('errorMessage',message)

    showLoading:->
      @spinner = @$('#loading').spin
        lines: 17 # The number of lines to draw
        length: 40 # The length of each line
        width: 2 # The line thickness
        radius: 44 # The radius of the inner circle
        corners: 1 # Corner roundness (0..1)
        rotate: 0 # The rotation offset
        direction: 1 # 1: clockwise, -1: counterclockwise
        color: "#000" # #rgb or #rrggbb or array of colors
        speed: 2.2 # Rounds per second
        trail: 41 # Afterglow percentage
        shadow: true # Whether to render a shadow
        hwaccel: false # Whether to use hardware acceleration
        className: "spinner" # The CSS class to assign to the spinner
        zIndex: 2e9 # The z-index (defaults to 2000000000)
        top: "50%" # Top position relative to parent
        left: "50%" # Left position relative to parent
      @$('#loading').show()

    hideLoading:->
      @$('#loading').hide()
