App.module "PjmToolsApp.Show.ViewModel", (ViewModel, App, Backbone, Marionette, $, _)->

  class ViewModel.InflowTimeResultModel extends Backbone.Model

    setInflowData:(dataPerf,dataPerfTrace, teamSolvers)->
      #ComputeTotalTime
      totalMinutes = 0
      projectCollection = new Backbone.Collection()

      dataPerfGroupedBy = dataPerf.groupBy('root_project')
      dataPerfTraceGrouped = dataPerfTrace.groupBy('root_project')
      debugger
      for name, perfs of dataPerfGroupedBy
        el = {}
        el.projectName = name
        filteredTeamAuthor = _.filter(perfs,(el)->
          _.any(teamSolvers,(e) -> el.get('solver').toUpperCase() == e) || teamSolvers == null
        )
        filteredNOTeamAuthor = _.filter(perfs,(el)->
          _.all(teamSolvers,(e) -> el.get('solver').toUpperCase() != e ) && teamSolvers != null
        )
        byAuthor = _.groupBy(filteredTeamAuthor,(el) -> return el.get('solver'))
        byAuthorNotInTeam = _.groupBy(filteredNOTeamAuthor, (el) -> return el.get('solver'))
        el.perfs = []
        el.perfsNotInTeam = []
        el.perfsInTeamTotal = 0
        el.perfsNotInTeamTotal = 0
        for author, perfAuthor of byAuthor
          time = perfAuthor.reduce(
                  (sum,el) => sum+el.get('performed_time')
                , 0)
          el.perfs.push
            name: author
            time: time

          el.perfsInTeamTotal+=time

        el.perfTotal = perfs.reduce(
          (sum,el) => sum+el.get('performed_time')
        , 0)

        el.showTeam = false

        for author, perfAuthor of byAuthorNotInTeam
          el.showTeam = true
          time = perfAuthor.reduce(
                  (sum,el) => sum+el.get('performed_time')
                , 0)
          el.perfsNotInTeam.push
            name: author
            time: time

          el.perfsNotInTeamTotal+=time


        perfTraces = dataPerfTraceGrouped[name]
        if(perfTraces != undefined)
          el.perfTracesTotal = perfTraces.reduce(
            (sum,el) => sum+el.get('performed_time')
          , 0)
        else
          el.perfTracesTotal = 0

        el.reserveInTeam = el.perfTracesTotal- el.perfsInTeamTotal
        el.reserveNotInTeam = el.perfTracesTotal-el.perfsNotInTeamTotal

        el.reserve = el.perfTracesTotal - el.perfTotal

        el.reserveOk = el.reserve >= 0

        projectCollection.add el

      @set('data',projectCollection.toJSON())
#      dataPerf.forEach (el)=>
#        time = el.get('performed_time')
#        if App.Utils.IsNumeric(time)
#          parsedTime = parseInt(time)
#
#          totalMinutes+= parsedTime
#          byAuthor = authorCollection.findWhere
#            solver: el.get('solver').toUpperCase()
#          unless(byAuthor)
#            byAuthor = new Backbone.Model(
#              solver: el.get('solver').toUpperCase()
#              totalTime: 0
#            )
#            authorCollection.add byAuthor
#
#          authorTime = byAuthor.get('totalTime')
#          authorTime+=parsedTime
#
#          byAuthor.set('totalTime', authorTime )
#
#      authorCollection.forEach (a)=>
#        time = a.get('totalTime')
#
#        a.set('totalManDays',(time/480).toFixed(1))
#        console.log ("author : #{a.get('solver')} / time : #{a.get('totalManDays')}")
#
#
    #  totalManDays = (totalMinutes/480).toFixed(1)
    #  console.log ("total man days : #{totalManDays}")
    #  @set('totalManDays',totalManDays)
    #  @set('byAuthors',authorCollection)


  class ViewModel.SelectorViewModel extends Backbone.Model

    defaults:
      'projectSelected':""
