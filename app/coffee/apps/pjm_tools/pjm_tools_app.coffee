App.module "PjmToolsApp", (PjmToolsApp, App, Backbone, Marionette, $, _)->


  API =
    showTools:->
      ctl = new PjmToolsApp.Show.Controller()
      ctl.show()

  class PjmToolsApp.Router extends Marionette.AppRouter
    appRoutes:
      "tools":"showTools"

  PjmToolsApp.on "start",=>
    new PjmToolsApp.Router
      controller: API

  App.vent.on "pjmtools:show", () =>
    App.navigate "tools"
    API.showTools()

