App.module "YoutrackConnectorApp.Model", (Model, App, Backbone, Marionette, $, _)->
  class Model.YoutrackIssue extends Backbone.Model

    initialize: ->
      @set('cust_summary', @getFieldValue("summary"))

    getFieldValue: (name)->
      fields = @get('field')
      found = _.findWhere(fields, name: name)
      if(found)
        found['value']
      else
        ''

  class Model.YoutrackIssues extends Backbone.Collection
    model: Model.YoutrackIssue

    comparator = 'id'

  class Model.YoutrackProject extends Backbone.Model

  class Model.YoutrackProjects extends Backbone.Collection
    model: Model.YoutrackProject

