App.module "YoutrackConnectorApp", (YoutrackConnectorApp, App, Backbone, Marionette, $, _)->
  class YoutrackConnectorApp.Controller extends Marionette.Controller

    initialize: ()->
      $.ajaxSetup
        headers:
          'Accept': 'application/json'
        contentType: "application/json"
        dataType: "json"

    login: ()->
      url = App.request 'preference:get', 'YoutrackUrl'
      login = App.request 'preference:get', 'YoutrackUser'
      password = App.request 'preference:get', 'YoutrackPassword'


      if(url)
        url = url.trimEnd('/')
        deferred = $.Deferred()
        $.ajax
          type: "POST"
          dataType: "xml"
          url: "#{url}/user/login?login=#{login}&password=#{password}"
          success: (data, status, xhr)->
            deferred.resolve()
          error: (data, status, xhr)->
            App.notify 'Youtrack connection ERROR'
            deferred.reject()
        deferred.promise()

    getProjects: ->
      url = App.request 'preference:get', 'YoutrackUrl'

      url = url.trimEnd('/')
      verbose = false
      deferred = $.Deferred()

      $.ajax
        type: "GET"
        url: "#{url}/project/all?verbose=#{verbose}"
        success: (data, status, xhr)->
          projectColl = new YoutrackConnectorApp.Model.YoutrackProjects(data)
          deferred.resolve(projectColl)
        error: (data, status, xhr)->
          App.notify 'Youtrack projects load failed'
          deferred.reject()

      deferred.promise()


#    /byproject/{project}?
    searchByProject: (projectName, filter)->
      url = App.request 'preference:get', 'YoutrackUrl'
      url = url.trimEnd('/')
      filter += ' order by: {issue id} desc' # add ordering

      url = "#{url}/issue/byproject/#{projectName}?filter=#{encodeURIComponent(filter)}&max=500"
      unless filter
        filter = ''
      deferred = $.Deferred()
      $.ajax
        type: "GET"
        url: url
        success: (data, status, xhr)->
          if(data)
            deferred.resolve(new YoutrackConnectorApp.Model.YoutrackIssues(data))
          else
            deferred.resolve(new YoutrackConnectorApp.Model.YoutrackIssues())
        error: (data, status, xhr)->
          App.notify 'Search youtrack failed !'
          return deferred.reject()

      deferred.promise()

    #POST /rest/issue/{issue}/timetracking/workitem
    uploadYoutrack: (details)->
      url = App.request 'preference:get', 'YoutrackUrl'
      url = url.trimEnd('/')
      moment = require('moment')

      xml = '<workItem>'
      xml += '<date>' + moment().unix() + '</date>'
      xml += '<duration>' + parseInt(details.duration) + '</duration>'
      xml += '<description>' + details.description + '</description>'
      xml += '</workItem>'

      deferred = $.Deferred()
      $.ajax
        type: 'POST'
        url: "#{url}/issue/#{details.issueId}/timetracking/workitem"
        dataType: 'text'
        contentType: 'text/xml'
        data: xml
      .done (data, status)->
        App.notify 'Youtrack upload success'
        deferred.resolve()
      .fail (jqxhr, status, errorThrown)->
        App.notify 'Youtrack Upload ERROR'
        deferred.fail()

      deferred.promise()
