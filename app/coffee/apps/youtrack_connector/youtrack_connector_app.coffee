App.module "YoutrackConnectorApp", (YoutrackConnectorApp, App, Backbone, Marionette, $, _)->

  YoutrackConnectorApp.on "start", ->
    @ctl = new YoutrackConnectorApp.Controller()

  App.reqres.setHandler "youtrack:login", ()=>
    @ctl.login()

  App.reqres.setHandler "youtrack:search", (filter)=>
    @ctl.search(filter)

  App.reqres.setHandler "youtrack:searchbyproject", (options)=>
    @ctl.searchByProject(options.project, options.filter)

  App.reqres.setHandler "youtrack:projects", ()=>
    @ctl.getProjects()

  App.reqres.setHandler "youtrack:upload", (details)=>
    @ctl.uploadYoutrack(details)

