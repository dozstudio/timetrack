App.module "NavigationApp.Show", (Show, App, Backbone, Marionette, $, _)->
  class Show.Controller extends Marionette.Controller

    showLinks: ()->
      @linksView = new Show.LinksView()
      App.linksRegion.show(@linksView)
      @linksView.on 'exit', App.exit

    selectLink: (link)->
      unless @linkView
        @showLinks()
      @linksView.selectLink(link)
