App.module "NavigationApp.Show", (Show, App, Backbone, Marionette, $, _)->
  class Show.LinksView extends Marionette.ItemView
    template: 'navigation/show/links'

    events:
      'click a': 'navigate'

    triggers:
      'click #btQuit': 'exit'

    navigate: (e, a)->
      e.preventDefault()
      href = @$(e.target).attr('href')
      App.hideBottomPanel()
      App.vent.trigger href

    selectLink: (link)->
      @$('li').removeClass('active')
      @$("a[href='#{link}']").parent().addClass('active')

    serializeData: ->
      adminMode: App.request('preference:get', 'AdminMode')
