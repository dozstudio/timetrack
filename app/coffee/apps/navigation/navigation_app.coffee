App.module "NavigationApp", (NavigationApp, App, Backbone, Marionette, $, _)->

  App.vent.on 'select:link', (link)=>
    @ctl =  new NavigationApp.Show.Controller()
    @ctl.selectLink(link)
