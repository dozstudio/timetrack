App.module "DashboardApp", (DashboardApp, App, Backbone, Marionette, $, _)->

  DashboardApp.on "start",=>
    @ctl = new DashboardApp.Show.Controller()

  App.vent.on "dashboard:show", (options) =>
    if(options)
      @ctl.showData(options.date,options.inflowUser,options.mode)
    else
      @ctl.showData()

