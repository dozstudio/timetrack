App.module "DashboardApp.Show", (Show, App, Backbone, Marionette, $, _)->
  class Show.ColorPickerView extends Marionette.ItemView
    template: 'dashboard/show/color_picker'

    events:
      'keyup #picker': 'escapePicker'

    initialize: (options)->
      @projectName = options.projectName
      @currentColor = options.color

    serializeData: ->
      name: @projectName
      color: @currentColor

    escapePicker:(e)->
      if (e.keyCode == 27 || e.keyCode == 13)
        @$('#picker').colorpicker('hide')
        @trigger('newColor',
          project: @projectName
          color: @$('#picker').val()
        )
        App.hideBottomPanel()

    onRender: ->
      @$('#picker').colorpicker().on 'changeColor.colorpicker', (event)=>
        @$('#picker').css('background-color', event.color.toHex())

