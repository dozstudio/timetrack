App.module "DashboardApp.Show", (Show, App, Backbone, Marionette, $, _)->

  class Show.DataView extends Marionette.CompositeView
    childView: Show.DataItemView
    childViewContainer: "#daysContainer"
    template: 'dashboard/show/data'

    triggers:
      'click #goToPrevious': 'goToPrevious'
      'click #goToNext': 'goToNext'
      'click #btRefresh': 'refresh'

    modelEvents:
      'change errorMessage': 'render'
      'dateCollectionItems': 'render'

    events:
      'keyup #usrInput': 'userInputKeyUp'
      'click #btWeekly': 'setWeeklyMode'
      'click #btMonthly': 'setMonthlyMode'
      'click #btYearly': 'setYearlyMode'
      'dblclick .cpicker': 'selectColor'

    bindings:
      '#usrInput':
        observe: 'inflowUser',
        setOptions:
          silent: true

    selectColor: (e)->
      el = e.target
      @trigger 'changeColor',
        project: @$(el).data('project')
        color: @$(el).data('color')


    setWeeklyMode: (e)->
      @$('#btWeekly').toggleClass('active')
      @$('#btMonthly').toggleClass('active')
      @$('#btYearly').toggleClass('active')
      @trigger('changeMode', 'weekly')

    setMonthlyMode: (e)->
      @$('#btWeekly').toggleClass('active')
      @$('#btMonthly').toggleClass('active')
      @$('#btYearly').toggleClass('active')
      @trigger('changeMode', 'monthly')

    setYearlyMode: (e)->
      @$('#btWeekly').toggleClass('active')
      @$('#btMonthly').toggleClass('active')
      @$('#btYearly').toggleClass('active')
      @trigger('changeMode', 'yearly')

    userInputKeyUp: (e)->
      if(e.keyCode == 13)
        @trigger('refresh')

    onBeforeRender: ->
      if @model && @model.get('dateCollectionItems')
        @collection = @model.get('dateCollectionItems')

    onRender: ->
      @$('[rel="tooltip"]').tooltip()
      if(@model.get('mode') == 'weekly')
        @$('#btWeekly').toggleClass('active')
      else if(@model.get('mode') == 'monthly')
        @$('#btMonthly').toggleClass('active')
      else if(@model.get('mode') == 'yearly')
        @$('#btYearly').toggleClass('active')
      @stickit()

#LOADING OPTIONS
    showLoading: ->
      @spinner = @$('#loading').spin
        lines: 17 # The number of lines to draw
        length: 40 # The length of each line
        width: 2 # The line thickness
        radius: 44 # The radius of the inner circle
        corners: 1 # Corner roundness (0..1)
        rotate: 0 # The rotation offset
        direction: 1 # 1: clockwise, -1: counterclockwise
        color: "#000" # #rgb or #rrggbb or array of colors
        speed: 2.2 # Rounds per second
        trail: 41 # Afterglow percentage
        shadow: true # Whether to render a shadow
        hwaccel: false # Whether to use hardware acceleration
        className: "spinner" # The CSS class to assign to the spinner
        zIndex: 2e9 # The z-index (defaults to 2000000000)
        top: "50%" # Top position relative to parent
        left: "50%" # Left position relative to parent
      @$('#loading').show()

    hideLoading: ->
      @spinner.stop()
