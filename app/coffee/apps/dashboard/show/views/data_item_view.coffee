App.module "DashboardApp.Show",(Show, App, Backbone, Marionette, $, _)->

  class Show.DataItemView extends Marionette.ItemView
    template: 'dashboard/show/dayItem'

    events:
      'click .barItem': 'clickItem'

    clickItem: (e)->
      rootid=$(e.target).data('projectid')
      moment = require('moment')

      retobj =
        rootid: rootid
        date: this.model.get('dateOfTheDay')
        
      @trigger('itemclick',retobj)

    onRender:->
      @$('[rel="tooltip"]').tooltip()
