App.module "DashboardApp.Show",(Show, App, Backbone, Marionette, $, _)->

  class Show.MainView extends Marionette.LayoutView
    template: 'dashboard/show/main'
    regions:
      weeklyRegion:'#weeklyRegion'
      monthlyRegion:'#monthlyRegion'
