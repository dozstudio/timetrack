App.module "DashboardApp.Show", (Show, App, Backbone, Marionette, $, _)->

  class Show.InflowPerfsDetailsTableView extends Marionette.ItemView
    template: 'dashboard/show/inflow_perf_detail_table_view'

    events:
      'click #closePopup': 'closePopup'

    closePopup: ->
      App.hideBottomPanel()

    serializeData: =>
      mom = require('moment')
      @collection.forEach (el)=>
        duration = mom.duration(el.get('performed_time'),'minutes')
        el.set('formatted_duration',duration.hours().toDigits(2) + 'h' + duration.minutes().toDigits(2))

      user: @model.user
      project: @model.root_node
      date: @model.date
      data : @collection.toJSON()
