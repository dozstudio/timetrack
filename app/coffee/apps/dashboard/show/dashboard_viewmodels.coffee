App.module "DashboardApp.Show.ViewModel", (ViewModel, App, Backbone, Marionette, $, _)->
  ViewModel.Colors = [
    "#ff0000",
    "#4b0082",
    "#008000",
    "#ffff00",
    "#0000ff",
    "#a52a2a",
    "#f0e68c",
    "#f5f5dc",
    "#00ffff",
    "#00008b",
    "#008b8b",
    "#a9a9a9",
    "#f0ffff",
    "#bdb76b",
    "#8b008b",
    "#556b2f",
    "#006400",
    "#ff8c00",
    "#9932cc",
    "#8b0000",
    "#e9967a",
    "#9400d3",
    "#ffd700",
    "#c0c0c0",
    "#ff00ff",
    "#add8e6",
    "#e0ffff",
    "#90ee90",
    "#d3d3d3",
    "#ffb6c1",
    "#ffffe0",
    "#00ff00",
    "#ff00ff",
    "#800000",
    "#000080",
    "#808000",
    "#ffa500",
    "#ffc0cb",
    "#800080",
    "#800080"
  ]

  ViewModel.ProjectsColors = {}

  ViewModel.on 'start', ()=>
    #Get preferences
    ViewModel.ProjectsColors = App.request 'preference:get', 'ProjectsColors'


  class ViewModel.DashboardWeekViewModel extends Backbone.Model

    defaults:
      'date': new Date()

    initialize: (startDate,endDate, user)->
      @moment = require("moment")
      @set('beginDate', startDate)
      @set('endDate',endDate)
      @set('adminMode', App.request('preference:get', 'AdminMode'))
      unless(user)
        user = App.request('preference:get', 'InflowUser')
      @set('inflowUser', user)

    createProjectColors: (data)=>
      ProjectsColors = {}
      byRootForColors = data.groupBy('root_project')

      for root,list of byRootForColors
        if(ViewModel.ProjectsColors[root] != undefined)
          ProjectsColors[root] = ViewModel.ProjectsColors[root]
          #remove from list
          ViewModel.Colors = _.without(ViewModel.Colors, ProjectsColors[root])
        else
          ProjectsColors[root] = ViewModel.Colors[0]
          ViewModel.ProjectsColors[root] = ViewModel.Colors[0]
          ViewModel.Colors = _.without(ViewModel.Colors, ProjectsColors[root])

      #Save prefs
      App.request 'preference:set',
        name: 'ProjectsColors'
        value: ProjectsColors

      ProjectsColors

    setNewColor:(color, project)->
      ViewModel.ProjectsColors[project] = color
      App.request 'preference:set',
        name: 'ProjectsColors'
        value: ViewModel.ProjectsColors

    setInflowData: (data)=>
#parse inflow data
      ProjectsColors = @createProjectColors(data)

      legend = []
      for pjName, color of ProjectsColors
        legend.push
          projectName: pjName
          color: color
          projectTotalTime: 0

      byDate = data.groupBy('date_s')

      dateCollectionItems = new ViewModel.DashboardWeekItemCollection()

      maxDuration = 0

      totalWeekDuration = 0

      startDate = @get('beginDate').clone().startOf('day').subtract(1, 'days')
      while startDate.format('DD-MM-YYYY') != @get('endDate').format('DD-MM-YYYY')
        startDate.add(1, 'days')
        dateForm = startDate.format('DD-MM-YYYY');
        console.log(dateForm);

        totalDayDuration = 0

        #Create date items collection
        dateItem = new ViewModel.DashboardWeekItemViewModel()
        dateItem.set('dateOfTheDay', startDate.format('ddd DD-MM-YYYY'))
        allProjectItems = new ViewModel.DashboardProjectItemCollection()

        items = byDate[startDate.format('DD-MM-YYYY')]

        if items
          items = new Backbone.Collection(items)
          byRoot = items.groupBy('root_project')

          for root,list of byRoot
            projectItem = new ViewModel.DashboardProjectItemViewModel()
            projectItem.set('project_name', root)
            projectItem.set('color', ProjectsColors[root])

            totalDurrInMinutes = 0
            for el in list
              totalDurrInMinutes += el.get('performed_time')
              totalDayDuration += el.get('performed_time')
              totalWeekDuration += el.get('performed_time')
              legendItem = _.findWhere(legend, projectName: root)
              if legendItem
                legendItem.projectTotalTime += el.get('performed_time')
            projectItem.set('perfs',list)
            projectItem.set('duration', totalDurrInMinutes)
            durationItem = @moment.duration(totalDurrInMinutes, 'minutes')
            projectItem.set('formatedDuration', "#{App.Utils.formatMinutesAsTime(totalDurrInMinutes)}")
            projectItem.set('projectName', root)
            projectItem.set('root_id',el.get('root_id'))
            allProjectItems.add(projectItem)

        if totalDayDuration > maxDuration
          maxDuration = totalDayDuration


        dateItem.set('totalDuration', totalDayDuration)

        dur = @moment.duration(totalDayDuration, 'minutes')

        dateItem.set('formatedTotalDuration', "#{App.Utils.formatMinutesAsTime(totalDayDuration)}")
        dateItem.set('projects', allProjectItems)
        dateCollectionItems.add(dateItem)


      for l in legend
        l.durationPercentage = Math.round((l.projectTotalTime / totalWeekDuration) * 100)
        l.formatedProjectTotalTime = "#{App.Utils.formatMinutesAsTime(l.projectTotalTime)}"

      legend = _.sortBy(legend, (l)->
        -l.projectTotalTime
      )

      i=0;
      legend = _.map(legend,(l)->
        l.order = i++
        return l
      )

      #parse all projects to set size of the progressbar
      for dateItem in dateCollectionItems.toArray()
        for item in dateItem.get('projects').toArray()
          durationOfTheItem = item.get('duration')
          finalPercentage = (100 / maxDuration) * durationOfTheItem
          item.set('percentage', finalPercentage)
          item.set('order',_.find(legend,(l)=>l.projectName == item.get('projectName')).order)
        dateItem.get('projects').sort()
        dateItem.set('projects_json', dateItem.get('projects').toJSON())

      @set('projectLegend', legend)
      @set('totalWeekDuration', totalWeekDuration)
      @set('formatedTotalWeekDuration', "#{App.Utils.formatMinutesAsTime(totalWeekDuration)}")
      @set('dateCollectionItems', dateCollectionItems)
      @set('maxDuration', maxDuration)


  class ViewModel.DashboardWeekItemViewModel extends Backbone.Model

  class ViewModel.DashboardWeekItemCollection extends Backbone.Collection
    model: ViewModel.DashboardWeekItemViewModel

  class ViewModel.DashboardProjectItemViewModel extends Backbone.Model

  class ViewModel.DashboardProjectItemCollection extends Backbone.Collection
    model: ViewModel.DashboardProjectItemViewModel

    comparator: (item)->
      App.log "compare "+JSON.stringify(item.toJSON(),2)
      return item.get('order')
