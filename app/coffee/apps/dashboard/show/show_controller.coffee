App.module "DashboardApp.Show", (Show, App, Backbone, Marionette, $, _)->
  class Show.Controller extends Marionette.Controller

    initialize: ->
      @mom = require('moment')

    showData: (date, user,mode)->

      unless mode
        mode = 'weekly'

      @currentMode = mode

      if(user)
        @currentUser = user
      else
        @currentUser = App.request('preference:get', 'InflowUser')

      App.vent.trigger 'select:link', 'dashboard:show'

      mainView = new Show.MainView()
      App.setMainView(mainView)

      if @currentMode == 'weekly'
        startDate = @mom(date).day(1);
        endDate = @mom(date).day(7);
      else if @currentMode == 'monthly'
        startDate = @mom(date).startOf('month')
        endDate = @mom(date).endOf('month')
      else if @currentMode == 'yearly'
        startDate = @mom(date).startOf('year')
        endDate = @mom(date).endOf('year')

      @viewModel = new Show.ViewModel.DashboardWeekViewModel(startDate,endDate, @currentUser)

      @viewModel.set('mode',@currentMode)

      @dataView = new Show.DataView
        model: @viewModel

      @dataView.on "childview:itemclick", (view,data)=>

        App.log "root id clicked #{data.rootid} for date #{data.date}"
        project = @viewModel.get('dateCollectionItems')
          .findWhere({dateOfTheDay:data.date}).get('projects')
          .findWhere({root_id:"#{data.rootid}"})
        perfs = project.get('perfs')
        perfsDetailsView =new Show.InflowPerfsDetailsTableView(
          collection: new Backbone.Collection(perfs)
          model:
            user: @viewModel.get('inflowUser')
            root_node: project.get('projectName')
            date: data.date
        )
        App.setBottomPanel(perfsDetailsView)


      @dataView.on "goToPrevious", =>
        date = @mom(date)

        if @currentMode == 'weekly'
          date.subtract(1, 'weeks')
        else if @currentMode == 'monthly'
          date.subtract(1, 'months')
        else if @currentMode == 'yearly'
          date.subtract(1,'years')
        App.vent.trigger "dashboard:show",
          date: date,
          inflowUser: @viewModel.get('inflowUser')
          mode: @currentMode

      @dataView.on "goToNext", =>
        date = @mom(date)
        if @currentMode == 'weekly'
          date.add(1, 'weeks')
        else if @currentMode == 'monthly'
          date.add(1, 'months')
        else if @currentMode == 'yearly'
          date.add(1,'years')
        App.vent.trigger "dashboard:show",
          date: date,
          inflowUser: @viewModel.get('inflowUser')
          mode: @currentMode

      @dataView.on "changeMode", (mode)=>
        App.vent.trigger "dashboard:show",
          date: date,
          inflowUser: @viewModel.get('inflowUser')
          mode: mode

      @dataView.on "refresh", =>
        @searchData()

      @dataView.on "changeColor", (data)=>
        console.log(data.project)
        console.log(data.color)
        colorPickerView = new Show.ColorPickerView
          projectName: data.project
          color: data.color
        colorPickerView.on('newColor',(data)=>
          App.log "new color #{data.color} for project #{data.project}"
          @viewModel.setNewColor(data.color,data.project)
          @searchData()
        )
        App.setBottomPanel(colorPickerView)

      mainView.weeklyRegion.show(@dataView)
      @searchData()

    searchData: ->
#show loading
      @dataView.showLoading()

      #query inflow
      $.when(App.request('inflow:select', @viewModel.get('beginDate'), @viewModel.get('endDate'),
          @viewModel.get('inflowUser'))
      ).done((data)=>
        @dataView.hideLoading()
        @viewModel.setInflowData(data)

      ).fail =>
        @viewModel.set('errorMessage', "Unable to connect to Inflow... Check your connection")


#when done sent date to current view Model
