class App extends Marionette.Application

  regions:
    mainRegion: '#main-region'
    linksRegion: '#links-region'
    footerRegion: '#footer-region'
    bottomPanelRegion: '#bottom-panel'

  initialize: =>
    @moment = require('moment')
    @hb_funcs = {}

  onStart: =>
    @log('Starting application')
    @startHistory()
    $(document).keyup (e)=>
      if (e.keyCode == 27)
        @hideBottomPanel()
    @log("setting up the heartbeat")
    @countStart = 0
    prevMinuteMoment = @moment()
    prevDayMoment = @moment()
    setInterval ()=>
      newMoment = @moment()

      unless prevMinuteMoment.format("m") == newMoment.format("m")
        prevMinuteMoment = newMoment
        @vent.trigger "hb:minute:changed"

      unless prevDayMoment.format("D") == newMoment.format("D")
        prevDayMoment = newMoment
        @vent.trigger "hb:day:changed"

      @countStart++
      if(@countStart % 1 == 0)
        @vent.trigger "hb:1s"
      if(@countStart % 5 == 0)
        @vent.trigger "hb:5s"
      if(@countStart % 10 == 0)
        @vent.trigger "hb:10s"
      if(@countStart % 15 == 0)
        @vent.trigger "hb:15s"
      if(@countStart % 30 == 0)
        @vent.trigger "hb:30s"
      if(@countStart % 60 == 0)
        @vent.trigger "hb:1m"
    , 1000

  refreshAllRegions: =>
    @log('Refresh all pages')
    regions = @getRegions()
    if(regions)
      for reg in Object.keys(regions)
        if(@[reg].currentView)
          @[reg].currentView.render()
    Backbone.history.loadUrl @getCurrentRoute()

  setMainView: (view)=>
    @log("Setting main view [#{view.cid}]")
    @mainRegion.show view

  setBottomPanel: (view)=>
    @log("Setting bottom panel view #{view.cid}")
    view.on 'show', =>
      @showBottomPanel()
    @bottomPanelRegion.show(view)

  notify: (message)=>
    @log "Notification : #{message}"
    $.gritter.add
      title: message

  exit: =>
    if confirm("Are you sure you want to quit the application")
      @log('Stopping application')
      ipc = require('ipc')
      ipc.send('quit')

  showBottomPanel: =>
    @log("Show bottom panel")
    $("#bottom-panel").removeClass('show')
    $("#bottom-panel").addClass('show')
    @vent.trigger 'bottom:panel:shown'

  hideBottomPanel: =>
    @log("Hide bottom panel")
    $("#bottom-panel").removeClass('show')
    @vent.trigger 'bottom:panel:hidden'

  navigate: (route, options = {}) ->
    route = "#" + route if route.charAt(0) is "/"
    Backbone.history.navigate route, options

  getCurrentRoute: ->
    frag = Backbone.history.fragment
    if _.isEmpty(frag) then null else frag

  startHistory: ->
    if Backbone.history
      Backbone.history.start()


  log: (message)=>
#    win = @gui.Window.get()
#    if win.isDevToolsOpen()
      console.log "#{@moment().format("HH:mm:ss:SSS")} : #{message}"

#Redefine renderer to use JST
Backbone.Marionette.Renderer.render = (templateName, data) =>
  unless templateName
    return ''
  if @JST[templateName]

    return @JST[templateName](data)
  else
    throw "The template #{templateName} is not found"

@App = do ->
  new App()
