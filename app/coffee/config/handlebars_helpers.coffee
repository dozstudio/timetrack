#register helpers from swag library

Handlebars.registerHelper 'formatDate',(date,format)->
  moment = require('moment')
  moment(date).format(format)

Handlebars.registerHelper 'asManDays',(time)->
  Math.round((time/480)*100)/100 + " Md"
