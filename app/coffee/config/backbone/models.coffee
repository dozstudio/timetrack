do (Backbone, $, _) ->
    _.extend Backbone.Model::,
        setDefaultField: (fieldName, def)->
            unless def
                def = ''
            unless @get(fieldName)
                @set(fieldName, def)
                return false
            return true

    _.extend(Backbone.Model.prototype, Backbone.Validation.mixin)
