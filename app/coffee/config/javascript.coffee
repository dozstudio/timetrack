Array::insertAt = (index, item) ->
    @splice(index, 0, item)
    @

# Extending Array's prototype
unless Array::filter
    Array::filter = (callback) ->
        element for element in this when callback(element)

jQuery.fn.exists = () ->
    @length > 0


String::trimStart = (c) ->
    return this  if @length is 0
    c = (if c then c else " ")
    i = 0
    val = 0
    while @charAt(i) is c and i < @length
        i++
    @substring i

String::trimEnd = (c) ->
    c = (if c then c else " ")
    i = @length - 1
    while i >= 0 and @charAt(i) is c
        i--
    @substring 0, i + 1

String::trim = (c) ->
    @trimStart(c).trimEnd c

String::leadingUpper = ->
    result = ""
    if @length > 0
        result += this[0].toUpperCase()
        result += @substring(1, @length)  if @length > 1
    result

Number::toDigits = (numberOfDigits)->
    s = @toString()
    while s.length < numberOfDigits
        s = "0" + s
    s

$.extend $.gritter.options,
    position: 'bottom-right'
    fade_in_speed: 'medium'
    fade_out_speed: 2000
    time: 2000
