# CoffeeScript
@App.Utils = {}

@App.Utils.setDefaultField = (obj, fieldName, def)->
    unless def
        def = ""

    unless obj.hasOwnProperty(fieldName)
        obj[fieldName] = def
        true

    false

@App.Utils.getUserHome = () ->
    switch process.platform
        when 'win32' then process.env['USERPROFILE']
        else
            process.env['HOME']

@App.Utils.formatMinutesAsTime = (durationInMinutes) ->
    moment = require('moment')
    durr = moment.duration(durationInMinutes, 'minutes')
    Math.floor(durr.asHours()).toDigits(2)+"h"+(durr.asMinutes()%60).toDigits(2)

@App.Utils.IsNumeric = (stringval)->
  !isNaN(parseFloat(stringval)) && isFinite(stringval)
