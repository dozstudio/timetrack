'use strict';

var gulp = require('gulp'),
  paths = gulp.paths,
  path = require('path'),
  builder = ( require('electron-builder') ).init(),
  $ = gulp.$,
  electronPackageJson = require('../node_modules/electron-prebuilt/package.json'),
  baseCwd = path.resolve(process.cwd()) + "/";

gulp.task('clean:release', function (done) {
  $.del([
    paths.dist
  ], done);
});

gulp.task('package:osx', ['build', 'clean:release'], $.shell.task([
  'node ./node_modules/.bin/electron-packager ./app \"' + gulp.appManifest.name + '\" --out=' + paths.dist + '/macos --platform=darwin --arch=x64 --version=' + electronPackageJson.version + ' --icon=icons/mac_icon.icns'
]));

gulp.task('package:win', ['build', 'clean:release'], $.shell.task([
  'node ./node_modules/.bin/electron-packager ./app \"' + gulp.appManifest.name + '\" --out=' + paths.dist + '/win --platform=win32 --arch=ia32 --version=' + electronPackageJson.version + ' --icon=icons/win_icon.ico'
]));

gulp.task('packages', ['package:osx', 'package:win']);

//    "pack:macos": "npm run build:macos && electron-builder \"dist/macos/TimeTrack.app\" --platform=macos --out=\"dist/macos\" --config=installconfig.json",
//    "pack:win": "npm run build:win && electron-builder \"dist/win/TimeTrack-win32\" --platform=win --out=\"dist/win\" --config=installconfig.json"

gulp.task('installer:osx', ['package:osx'], function (done) {
  builder.build({
    appPath: baseCwd + paths.dist + "/macos/" + gulp.appManifest.name + ".app",
    platform: 'macos',
    out: paths.dist + "/macos/",
    basePath: baseCwd,
    config: {
      "macos": {
        "title": gulp.appManifest.name + "_" + gulp.appManifest.version + "_Setup",
        "background": "icons/installer.jpg",
        "icon": "icons/mac_icon.icns",
        "icon-size": 80,
        "contents": [
          {"x": 438, "y": 100, "type": "link", "path": "/Applications"},
          {"x": 192, "y": 100, "type": "file"}
        ]
      },
    }
  }, function (error) {
    if (error)
      $.util.log('Error while creating installer : ' + error);
    else {
      done()
    }
  });
});

gulp.task('installer:win', ['package:win'], function (done) {
  builder.build({
    appPath: baseCwd + paths.dist + "/win/" + gulp.appManifest.name + "-win32",
    platform: 'win',
    out: paths.dist + "/win/",
    basePath: baseCwd,
    config: {
      "win" : {
        "title" :  gulp.appManifest.name,
        "icon" : "icons/win_icon.ico"
      },
    }
  }, function (error) {
    if (error)
      $.util.log('Error while creating installer : ' + error);
    else {
      done()
    }
  });
});

gulp.task('clean:apps',['installer:osx', 'installer:win'],function(done){
  $.del([
    baseCwd + paths.dist + "/macos/" + gulp.appManifest.name + ".app",
    baseCwd + paths.dist + "/win/" + gulp.appManifest.name + "-win32",
  ],done);
})

gulp.task('installers', ['installer:osx', 'installer:win', 'clean:apps']);