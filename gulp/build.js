'use strict';

var gulp = require('gulp'),
  paths = gulp.paths,
  path = require('path'),
  $ = gulp.$;

gulp.task('clean:js:full',function(done){
  $.del([
    paths.app + '/js/**/'
  ], done);
})

gulp.task('clean:js', function (done) {
  $.del([
    paths.app + '/js/**/*.js','!'+paths.app+'/js/templates.js'
  ], done);
});

gulp.task('clean:bower', function(done){
  $.del([
    paths.app + "/vendor/bower/**"
  ],done);
});

gulp.task('clean:templates', function (done) {
  $.del([
    paths.app + '/js/templates.js',
  ], done);
});

gulp.task('clean:css', function (done) {
  $.del([
    paths.app + '/css',
  ], done);
});

gulp.task('clean',['clean:js:full','clean:bower','clean:css']);

gulp.task('coffee', ['clean:js:full'], function () {
  return gulp.src(paths.app + '/coffee/**/*.coffee')
    .pipe($.coffee({
      bare: false
    }).on('error', $.util.log))
    .pipe(gulp.dest(paths.app + '/js/'));
});

gulp.task('less', ['clean:css'],function () {
  return gulp.src(paths.app + '/less/**/*.less')
    .pipe($.less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest(paths.app+'/css/'))
});

gulp.task('templates', ['clean:templates'], function () {
  gulp.src(paths.app + '/**/*.hbs', {
    base: paths.app + '/coffee/apps'
  })
    .pipe($.handlebars({
      handlebars: require('handlebars')
    }))
    .pipe($.wrap('Handlebars.template(<%= contents %>)'))
    .pipe($.declare({
      namespace: 'JST',
      noRedeclare: true, // Avoid duplicate declarations
      processName: function (filePath) {
        var processNameByPath = $.declare.processNameByPath(filePath.replace('app/coffee/apps/', ''));
        processNameByPath = processNameByPath.replace('app.coffee.apps.','');
        processNameByPath = processNameByPath.replace(/\./g, "/").replace("/templates", "");
        return processNameByPath;
      }
    }))
    .pipe($.concat('templates.js'))
    .pipe(gulp.dest(paths.app + '/js/'));
});



gulp.task('inject:app',['coffee','less','templates'],function(){
  var sources = gulp.src([
      paths.app + "/css/**/*.css",
      paths.app + "/js/templates.js",
      paths.app + "/js/app.js",
      paths.app + "/js/config/**/*.js",
      paths.app + "/js/apps/**/*.js",
      paths.app + "/js/startup.js"
    ], {
      read: false
    });

  return gulp.src(paths.app + '/index.html')
    .pipe($.inject(sources, {
      relative: 'true'
    }))
    .pipe(gulp.dest(paths.app));
});

gulp.task('inject:bower',['clean:bower','inject:app'],function(){
  var bowerSources = gulp.src($.mainBowerFiles(), {
    base: "./bower_components/"
  }).pipe(gulp.dest(paths.app + '/vendor/bower'));

  return gulp.src(paths.app + '/index.html')
    .pipe($.inject(bowerSources, {
      name: 'bower',
      relative: 'true'
    }))
    .pipe(gulp.dest(paths.app));

});

gulp.task('build', ['clean:js:full','inject:app','inject:bower']);

