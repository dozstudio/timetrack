'use strict';

var gulp = require('gulp'),
  paths = gulp.paths,
  $ = gulp.$;

gulp.task('deploy', ['installers'], function () {
  return gulp.src(paths.dist+'/**')
    .pipe($.ftp({
      host: 'dozstudio.ddns.net',
      user: 'timetrack',
      pass: 'timetrack2015',
      remotePath: '/' + gulp.appManifest.version + '/'
    }))
    // you need to have some kind of stream after gulp-ftp to make sure it's flushed
    // this can be a gulp plugin, gulp.dest, or any kind of stream
    // here we use a passthrough stream
    .pipe($.util.noop());
});
