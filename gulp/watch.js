'use strict';

var gulp = require('gulp'),
    paths = gulp.paths,
    utils = require('./utils'),
    $ = gulp.$;

gulp.task('watch',function(){
  $.watch(
    [
      './app/coffee/**/*.coffee',
      './app/less/**/*.less',
      './app/coffee/**/*.hbs',
    ],
    function(vinyl){
      $.util.log('Watched File => ', vinyl.path);
      gulp.start('inject:app');

    }
  );
  gulp.start('run');
});
