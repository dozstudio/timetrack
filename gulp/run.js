'use strict';

var gulp = require('gulp'),
  paths = gulp.paths,
  $ = gulp.$;

var exec = require('child_process').exec;



var isWin = /^win/.test(process.platform);

if(isWin){
  gulp.task('run', ['build'],function(cb){
    exec (".\\node_modules\\.bin\\electron.cmd .\\app", function(err,stdout,stderr){
      console.log(stdout);
      console.log(stderr);
      cb(err);
    });
  });
}else{
  gulp.task('run', ['build'],$.shell.task([
    'node ./node_modules/.bin/electron ./app'
  ]));
}

