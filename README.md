#Time Tracker

Personnal Time Tracking tool

##Pre-Requisites

- **Node Js** must be installed on your system  ([Getting node](http://nodejs.org/))

##Installation Instructions

```
git clone https://dozstudio@bitbucket.org/dozstudio/timetrack.git
cd timetrack
npm install
```

## Run & Debug application
```
npm start
```

## Packaging application

### Create applications for win and osx
```
npm run packages
```

### Create setup for osx and win
```
npm run installers
```

### Deploy setups to remote ftp 
```
npm run deploy
```

## Cleaning up folders 

> You will need to redo a `npm install` after to get a full working system

```
npm run clean
```

Compiled setups for win & osx are stored in `/dist/`folder