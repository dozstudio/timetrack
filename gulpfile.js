'use strict';

var gulp = require('gulp');

gulp.paths = {
  app: 'app',
  dist: 'dist',
  tmp: 'tmp'
};

gulp.appManifest = require('./app/package.json');

gulp.$ = require('gulp-load-plugins')({
  pattern:['gulp-*','main-bower-files','del','wiredep']
});

require('require-dir')('./gulp');

